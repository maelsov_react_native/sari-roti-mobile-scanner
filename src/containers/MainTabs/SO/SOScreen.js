import React, { useState, useEffect, useRef } from 'react'
import { View, Text, TextInput, StyleSheet, TouchableOpacity, FlatList, RefreshControl, Image } from 'react-native'
import { Actions } from 'react-native-router-flux';

import { Colors, Fonts, Icons, Illustrations, Metrics, StorageKeys } from '../../../globals/GlobalConfig'
import GlobalStyle from '../../../globals/GlobalStyle';
import Ionicons from 'react-native-vector-icons/Ionicons';

import CustomToast from '../../../components/CustomToast';
import CustomCalendar from '../../../components/CustomCalendar';
import { wait } from '../../../globals/GlobalFunction';
import { modelTransactions } from '../../../models/Transaction';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

const CardDisplay = (props) => {
	const { data } = props
	const {
		TR_PID_HEADER_ID,
		TR_PID_HEADER_SAP_NO,
		TR_PID_HEADER_SAP_CREATED_DATE,
		TR_PID_HEADER_YEAR,
		TR_PID_HEADER_PLANT,
	} = data

	const openDetailPID = () => {
		Actions.detailSO({ pidHeaderNumber: TR_PID_HEADER_SAP_NO, headerID: TR_PID_HEADER_ID })
	}
	return (
		<TouchableOpacity style={styles.cardContainer} onPress={() => openDetailPID()}>
			<View style={{ flex: 3 }}>
				<Text style={styles.cardTitleText}>Document SAP</Text>
				<Text style={styles.cardTitleText}>Document Year</Text>
				<Text style={styles.cardTitleText}>Plant</Text>
				<Text style={styles.cardTitleText}>Created Date</Text>
			</View>
			<View style={{ flex: 2, alignItems: "flex-end" }}>
				<Text style={styles.cardDescriptionText}>{TR_PID_HEADER_SAP_NO}</Text>
				<Text style={styles.cardDescriptionText}>{TR_PID_HEADER_YEAR}</Text>
				<Text style={styles.cardDescriptionText}>{TR_PID_HEADER_PLANT}</Text>
				<Text style={styles.cardDescriptionText}>{TR_PID_HEADER_SAP_CREATED_DATE}</Text>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const StockOpnameScreen = (props) => {
	const { lastUpdate } = props
	const [isLoading, setIsLoading] = useState(false)
	const [isStartDate, setIsStartDate] = useState(false)
	const [incorrectDate, setIncorrectDate] = useState(null)
	const [incorrectMessage, setIncorrectMessage] = useState('')
	const [isDateModalVisible, setIsDateModalVisible] = useState(false)
	const [dataSO, setDataSO] = useState([])
	const [filteredSO, setFilteredSO] = useState([])
	const toastRef = useRef(null);
	const [search, setSearch] = useState('')
	const [startDate, setStartDate] = useState(moment(new Date).startOf('month').format('YYYY-MM-DD'))
	const [endDate, setEndDate] = useState(moment(new Date).format('YYYY-MM-DD'))

	useEffect(() => {
		const keys = [
			StorageKeys.PID_DATA_SUBMIT,
			StorageKeys.PID_SUBMITTED,
		]
		AsyncStorage.multiRemove(keys)
		initialLoad()
	}, [lastUpdate])

	const initialLoad = (data, data2) => {
		const { current } = toastRef
		setIsLoading(true)
		wait(2000).then(() => {
			setIsLoading(true)
			let params_data = `start_date=${data != null ? data : startDate}&end_date=${data2 != null ? data2 : endDate}`
			modelTransactions.getPIDView(params_data, res => {
				const { status, result } = res
				console.log("PID VIEW", result)
				console.log(res)
				switch (status) {
					case 200:
						setDataSO(result.data)
						setFilteredSO(result.data)
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const clearSO = () => {
		setSearch("")
		setFilteredSO(dataSO)
	}

	const onSOSearch = value => {
		let filteredList = []
		setSearch(value)
		for (let i = 0; i < dataSO.length; i++) {
			if (String(dataSO[i].TR_PID_HEADER_ID).toUpperCase().includes(String(value).toUpperCase()) ||
				String(dataSO[i].TR_PID_HEADER_SAP_NO).toUpperCase().includes(String(value).toUpperCase())) filteredList.push(dataSO[i])
			if (i === dataSO.length - 1) setFilteredSO(filteredList)
		}
	}

	const funcGetDate = (res) => {
		if (!isStartDate) {
			if (res.dateString > endDate) {
				setStartDate(moment().startOf('month').format('YYYY-MM-DD'))
				setEndDate(moment(new Date).format('YYYY-MM-DD'))
				setIsStartDate(false)
				setIncorrectDate(new Date)
				setIncorrectMessage("Start date must be smaller than end date")
			} else {
				setStartDate(res.dateString)
				setIsStartDate(true)
			}
		}
		else {
			if (startDate > res.dateString) {
				setStartDate(moment().startOf('month').format('YYYY-MM-DD'))
				setEndDate(moment(new Date).format('YYYY-MM-DD'))
				setIsStartDate(false)
				setIncorrectDate(new Date)
				setIncorrectMessage("Start date must be smaller than end date")
			}
			else {
				setEndDate(res.dateString)
				setIsStartDate(false)
				setIsDateModalVisible(false)
				initialLoad(startDate, res.dateString)
			}
		}
	}

	return (
		<View style={{ flex: 1 }}>
			<View style={{ paddingTop: Metrics.SAFE_AREA, width: '100%', backgroundColor: Colors.WHITE, elevation: 2 }}>
				<View style={{ width: '100%', paddingHorizontal: Metrics.SAFE_AREA, height: 60, paddingBottom: 10, justifyContent: 'center', marginLeft: -2, marginBottom: 10 }}>
					<View style={{ flex: 1, flexDirection: 'row', borderRadius: 50, backgroundColor: Colors.GRAY_LIGHT, overflow: 'hidden', paddingRight: 10 }}>
						<View style={{ height: '100%', justifyContent: 'center', marginHorizontal: 10 }}>
							<Ionicons name="search-outline" size={18} color="black" />
						</View>
						<TextInput
							placeholder='Cari Stock Opname'
							placeholderTextColor={Colors.GRAY}
							value={search}
							style={{ flex: 1, fontSize: 14, fontFamily: Fonts.ROBOTO_REGULAR, color: Colors.BLACK }}
							onChangeText={onSOSearch}
							returnKeyType='search'
						/>
						{search !== '' ?
							<TouchableOpacity onPress={clearSO}>
								<View style={{ height: '100%', justifyContent: 'center', marginLeft: 10 }}>
									<Ionicons name="close-circle-outline" size={24} color="black" />
								</View>
							</TouchableOpacity> : null}
					</View>
				</View>
			</View>
			<TouchableOpacity
				onPress={() => setIsDateModalVisible(true)}
				style={styles.dateStyle}>
				<Text style={{ fontSize: 14, fontFamily: Fonts.ROBOTO_REGULAR, letterSpacing: 0.5, lineHeight: 24, color: Colors.SemiBlackColor }}>Select Start Date</Text>
				<View style={{ alignItems: 'center', flexDirection: 'row' }}>
					<Ionicons name="calendar-outline" size={22} color="black" />
					<Text style={{ fontSize: 14, fontFamily: Fonts.ROBOTO_REGULAR, letterSpacing: 0.5, lineHeight: 24, color: Colors.SemiBlackColor }}> {moment(startDate).format('DD MMMM')} - {moment(endDate).format('DD MMMM')}</Text>
				</View>
			</TouchableOpacity>
			<FlatList
				refreshControl={(
					<RefreshControl
						refreshing={isLoading}
						onRefresh={initialLoad}
					/>
				)}
				data={filteredSO}
				extraData={filteredSO}
				keyExtractor={item => String(item.TR_PID_HEADER_ID)}
				contentContainerStyle={GlobalStyle.viewContainer}
				// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
				onEndReachedThreshold={0.1}
				ListEmptyComponent={(
					<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
							<View style={styles.illustContainerImage}>
								<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
								<View style={styles.titleTextContainer}>
									<Text style={styles.illustTextDesc}>Stock Opname is empty</Text>
								</View>
							</View>
					</View>
				)}
				renderItem={({ item, index }) => (
					<CardDisplay
						data={item} />
				)}
			// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
			// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
			/>
			<CustomCalendar
				message={incorrectMessage}
				lastUpdate={incorrectDate}
				isStartDate={isStartDate}
				onBackdropPress={() => setIsDateModalVisible(false)}
				currentDate={startDate}
				endDate={endDate}
				funcGetDate={funcGetDate}
				isDateModalVisible={isDateModalVisible}
			/>
			<CustomToast ref={toastRef} />
		</View >
	)
}

const styles = StyleSheet.create({
	dateStyle: {
		backgroundColor: Colors.WhiteColor,
		height: 60,
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		borderBottomWidth: 3,
		borderBottomColor: Colors.GRAY_LIGHT,
	},
	cardContainer: {
		flexDirection: "row",
		marginBottom: 20,
		marginHorizontal: 5,
		paddingVertical: 10,
		paddingHorizontal: 15,
		borderRadius: 5,
		backgroundColor: Colors.WHITE,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.50,
		shadowRadius: 1.45,
		elevation: 2,
	},
	cardTitleText: {
		fontSize: 14,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: 'bold'
	},
	cardDescriptionText: {
		fontSize: 12,
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontWeight: 'bold',
		marginBottom: 4
	},
	illustContainerImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    illustStyle: {
        width: 200,
        height: 200,
        resizeMode: 'cover',
    },
    illustTextDesc: {
        fontFamily: Fonts.ROBOTO_REGULAR,
        fontSize: 14,
        textAlign: 'center',
        marginTop: 20,
        color: Colors.BLACK,
    },
})

export default StockOpnameScreen