import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, View, Text, StyleSheet, FlatList, TouchableOpacity, Image, RefreshControl } from 'react-native'
import { Actions } from 'react-native-router-flux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';

import { Colors, Fonts, Icons, Illustrations, Metrics, StorageKeys } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { getPIDDataSubmit, getSubmittedPID, wait } from '../../../../globals/GlobalFunction';
import { modelTransactions } from '../../../../models/Transaction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomButton from '../../../../components/CustomButton';
import CustomToast from '../../../../components/CustomToast';
import CustomModalConfirm from '../../../../components/CustomModalConfirm';

const CardDisplay = (props) => {
	const { data, materialData, TR_PID_DETAIL_ID } = props
	const {
		TR_GR_DETAIL_MATERIAL_NAME,
		TR_GR_DETAIL_MATERIAL_CODE,
		TR_GR_DETAIL_LEFT_QTY,
		TR_GR_DETAIL_UNLOADING_PLANT,
		TR_GR_DETAIL_ID,
		TR_GR_DETAIL_BASE_UOM,
		TR_GR_DETAIL_SLOC,
	} = data

	const isChecked = materialData.findIndex(e => e.GRDetailID == TR_GR_DETAIL_ID) > -1

	const found = materialData.find(e => e.GRDetailID == TR_GR_DETAIL_ID)

	const goToDetailMaterial = () => {
		if (isChecked) {
			Actions.detailMaterialSO2({ pidDetailID: TR_PID_DETAIL_ID, GRDetailID: TR_GR_DETAIL_ID, materialCode: TR_GR_DETAIL_MATERIAL_CODE, materialName: TR_GR_DETAIL_MATERIAL_NAME, foto: found.photo, foto2: found.photoTemp, note: found.note, qty: found.qty, uom: found.uom, submited: true, storageLocation: TR_GR_DETAIL_SLOC, batchNumber: found.batchNumber })
		}
		else {
			Actions.detailMaterialSO2({ pidDetailID: TR_PID_DETAIL_ID, GRDetailID: TR_GR_DETAIL_ID, materialCode: TR_GR_DETAIL_MATERIAL_CODE, materialName: TR_GR_DETAIL_MATERIAL_NAME, uom: TR_GR_DETAIL_BASE_UOM, storageLocation: TR_GR_DETAIL_SLOC })
		}
	}

	return (
		<TouchableOpacity style={{ flexDirection: "row", paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: Colors.GRAY_DARK, marginBottom: 20 }} onPress={() => goToDetailMaterial()}>
			<View style={{ flex: 4 }}>
				<CustomInputComponent
					disabled
					label='Material'
					value={TR_GR_DETAIL_MATERIAL_NAME}
				/>
				<CustomInputComponent
					disabled
					label='Material Code'
					value={TR_GR_DETAIL_MATERIAL_CODE}
				/>
				<CustomInputComponent
					disabledTextInput
					suffix={TR_GR_DETAIL_BASE_UOM}
					label='Quantity Left'
					value={TR_GR_DETAIL_LEFT_QTY}
				/>
				<CustomInputComponent
					disabled
					label='Plant'
					value={TR_GR_DETAIL_UNLOADING_PLANT}
				/>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				{isChecked && (
					<FontAwesome
						name='check'
						color={Colors.GREEN}
						size={16}
						style={{ position: "absolute", top: 5, right: 5, height: 20, width: 20 }}
					/>
				)
				}
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const DetailMaterialStockOpname = (props) => {
	const { lastUpdate, pidLineMaterial, pidDetailID, submited } = props
	const [scrollPosition, setScrollPosition] = useState(0)
	const [isLoading, setIsLoading] = useState(false)

	const [materialList, setMaterialList] = useState([])
	const [submitedMaterialList, setSubmitedMaterialList] = useState([])
	const [filteredMaterialList, setFilteredMaterialList] = useState([])
	const [filteredMaterialListIndex, setFilteredMaterialListIndex] = useState("5")

	const [isModalConfirmVisible, setIsModalConfirmVisible] = useState(false)
	const toastRef = useRef(null);

	useEffect(() => {
		if (lastUpdate) {
			getPIDDataSubmit().then(result => {
				if (result != null) {
					console.log("submited PID", result)
					setSubmitedMaterialList(result)
				}
			})
		}
		initialLoad()
	}, [lastUpdate])

	const initialLoad = () => {
		setIsLoading(true)
		wait(2000).then(() => {
			let params_data = `pid_detail_id=${pidDetailID}`
			modelTransactions.getPIDDetailMaterial(params_data, res => {
				const { status, result } = res
				console.log("MATERIAL DETAIL", result.data)
				switch (status) {
					case 200:
						setMaterialList(result.data)
						if (result.data.length > 5) {
							setFilteredMaterialList(result.data.slice(0, filteredMaterialListIndex))
						}
						else {
							setFilteredMaterialList(result.data)
						}
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const handleSubmitPID = async () => {
		const { current } = toastRef
		const jumlahData = materialList.reduce((acc, cur) => {
			const found = submitedMaterialList.findIndex(e => e.GRDetailID == cur.TR_GR_DETAIL_ID) > -1
			if (found) return acc + 1
			else acc
		}, 0)

		const goodData = jumlahData == materialList.length
		console.log(jumlahData)
		console.log(materialList.length)
		console.log(goodData)
		if (goodData) {
			setIsModalConfirmVisible(true)
		}
		else {
			current.showToast('warning', "Mohon submit material lainnya!")
		}
	}

	const onSubmitGR = () => {
		const { current } = toastRef
		getSubmittedPID().then(result => {
			const data = {
				pidLineMaterial: pidLineMaterial,
			};
			if (result != null) {
				let PIDData = [...result, data]
				AsyncStorage.setItem(StorageKeys.PID_SUBMITTED, JSON.stringify(PIDData));
			}
			else {
				AsyncStorage.setItem(StorageKeys.PID_SUBMITTED, JSON.stringify([data]));
			}
		});
		setIsModalConfirmVisible(false)
		setIsLoading(true)
		wait(2000).then(() => {
			current.showToast('success', "Material has been submitted successfully!")
			wait(2000).then(() => {
				setIsLoading(false)
				Actions.pop()
			})
		})
	}

	const loadOtherIndex = () => {
		setIsLoading(true)
		const index = parseInt(filteredMaterialListIndex) + 5
		if (index < materialList.length) {
			setFilteredMaterialListIndex(index)
			setFilteredMaterialList(materialList.slice(0, index))
			setIsLoading(false)
		}
		else if (index >= materialList.length) {
			setFilteredMaterialListIndex(materialList.length)
			setFilteredMaterialList(materialList.slice(0, materialList.length))
			setIsLoading(false)
		}
	}

	const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
		const paddingToBottom = 20;
		return layoutMeasurement.height + contentOffset.y >=
			contentSize.height - paddingToBottom;
	};

	return (
		<View style={{ flex: 1 }}>
			<ScrollView>
				<View style={GlobalStyle.formContentContainer}>
					<Text style={GlobalStyle.formHeaderTitleText}>List of Material ({materialList.length})</Text>
					<View style={{ marginTop: 10 }}>
						<FlatList
							nestedScrollEnabled={true}
							style={{ maxHeight: Metrics.SCREEN_WIDTH * 2 }}
							onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}
							onScrollEndDrag={({ nativeEvent }) => {
								if (isCloseToBottom(nativeEvent) && materialList.length > filteredMaterialListIndex && isLoading === false) {
									loadOtherIndex()
								}
							}}
							scrollEventThrottle={100}
							refreshControl={(
								<RefreshControl
									refreshing={isLoading}
									onRefresh={initialLoad}
								/>
							)}
							data={filteredMaterialList}
							extraData={filteredMaterialList}
							keyExtractor={item => String(item.TR_GR_DETAIL_ID)}
							contentContainerStyle={GlobalStyle.viewContainer}
							// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
							onEndReachedThreshold={0.1}
							ListEmptyComponent={(
								<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
									<View style={styles.illustContainerImage}>
										<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
										<View style={styles.titleTextContainer}>
											<Text style={styles.illustTextDesc}>Material is empty</Text>
										</View>
									</View>
								</View>
							)}
							renderItem={({ item, index }) => (
								<CardDisplay
									TR_PID_DETAIL_ID={pidDetailID}
									materialData={submitedMaterialList}
									data={item} />
							)}
						// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
						// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
						/>
					</View>
				</View>

				<View style={[GlobalStyle.formButtonContainer, { flexDirection: "row" }]}>
					{!submited && materialList.length != 0 &&
						<CustomButton
							onPress={handleSubmitPID}
							isLoading={isLoading}
							label='Submit'
						/>
					}
					<View style={{ width: 10 }}></View>
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						isLoading={isLoading}
						label='Back'
					/>
				</View>
			</ScrollView>
			<CustomModalConfirm
				title='Submit Material'
				body='Are you sure want to submit Material? Make sure your data are correct.'
				onConfirm={onSubmitGR}
				onCancel={() => setIsModalConfirmVisible(false)}
				isModalVisible={isModalConfirmVisible}
			/>
			<CustomToast ref={toastRef} />
		</View>
	)
}

export default DetailMaterialStockOpname

const styles = StyleSheet.create({
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	illustContainerImage: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	illustStyle: {
		width: 200,
		height: 200,
		resizeMode: 'cover',
	},
	illustTextDesc: {
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontSize: 14,
		textAlign: 'center',
		marginTop: 20,
		color: Colors.BLACK,
	},
})