import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, View, StyleSheet, Image } from 'react-native'
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';

import { Colors, Fonts, StorageKeys } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { getPIDDataSubmit, inputValidation } from '../../../../globals/GlobalFunction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomModalCamera from '../../../../components/CustomModalCamera';
import CustomModalScanBarcode from '../../../../components/CustomModalScanBarcode';
import CustomButton from '../../../../components/CustomButton';
import CustomToast from '../../../../components/CustomToast';
import CustomModalConfirm from '../../../../components/CustomModalConfirm';
import { modelTransactions } from '../../../../models/Transaction';

const DetailMaterialStockOpname2 = (props) => {
	const { note, qty, submited, foto2, pidDetailID, batchNumber, GRDetailID } = props
	const [scrollPosition, setScrollPosition] = useState(0)
	
	const [catatan, setCatatan] = useState(submited ? note : '')
	const [materialQuantity, setMaterialQuantity] = useState(submited ? qty : '')
	const [tempReceiverIDPhoto, setTempReceiverIDPhoto] = useState(submited ? foto2 : '')
	const [tempReceiverBarcode, setTempReceiverBarcode] = useState(submited ? batchNumber : '')
	const [fotoBase64AbsensiList, setFotoBase64AbsensiList] = useState('')
	const [UOM, setUOM] = useState(props.uom)
	const [materialCode, setMaterialCode] = useState(props.materialCode)
	const [materialName, setMaterialName] = useState(props.materialName)
	const [storageLocation, setStorageLocation] = useState(props.storageLocation)

	const [isLoading, setIsLoading] = useState(false)
	const [barcodeIsScanned, setBarcodeIsScanned] = useState(false)
	const [isModalConfirmVisible, setIsModalConfirmVisible] = useState(false)
	const [isModalTakePhotoVisible, setIsModalTakePhotoVisible] = useState(false)
	const [isModalScanBarcodeVisible, setIsModalScanBarcodeVisible] = useState(false)

	const toastRef = useRef(null);

	useEffect(() => {
		initialLoad()
		// console.log(materialCode)
		console.log("!", GRDetailID)
	}, [])

	const initialLoad = () => {

	}

	const handleSubmitMaterial = () => {
		const { current } = toastRef

		if (materialQuantity == '') {
			current.showToast('warning', "Mohon input kuantitas!")
		}
		else if (catatan === '') {
			current.showToast('warning', "Mohon input catatan!")
		}
		// else if (tempReceiverIDPhoto === '') {
		// 	current.showToast('warning', "Mohon ambil foto!")
		// }
		else {
			setIsModalConfirmVisible(true)
		}
	}

	const onSubmitMaterial = () => {
		getPIDDataSubmit().then(result => {
			const data = {
				pidDetailID: pidDetailID,
				GRDetailID: GRDetailID,
				uom: UOM,
				qty: materialQuantity,
				photo: fotoBase64AbsensiList,
				note: catatan,
				photoTemp: tempReceiverIDPhoto,
				batchNumber: tempReceiverBarcode,
			};
			if (result != null) {
				let PIDData = [...result, data]
				AsyncStorage.setItem(StorageKeys.PID_DATA_SUBMIT, JSON.stringify(PIDData));
			}
			else {
				AsyncStorage.setItem(StorageKeys.PID_DATA_SUBMIT, JSON.stringify([data]));
			}
		});
		setIsModalConfirmVisible(false)
		Actions.pop()
	}

	const onPictureTaken = (cameraData) => {
		const {
			uri,
			base64
		} = cameraData

		setFotoBase64AbsensiList(base64)
		setTempReceiverIDPhoto(uri)
		setIsModalTakePhotoVisible(false)
	}

	const onScanBarcode = (barcodeData) => {
		const { current } = toastRef
		let params_data = `TR_GR_DETAIL_QR_CODE_NUMBER=${barcodeData}`
		modelTransactions.getPIDScanQR(params_data, res => {
			const { status, result } = res
			console.log(res)
			console.log(GRDetailID)
			console.log(result.data.gr_detail_data)
			switch (status) {
				case 200:
					if(result.data.gr_detail_data.length==0 || result.data.gr_detail_data.TR_GR_DETAIL_ID != GRDetailID){
						current.showToast('warning', "Mohon pindai QR Code yang valid")
						setIsModalScanBarcodeVisible(false)
						setTempReceiverBarcode('')
					}
					else{
						setTempReceiverBarcode(barcodeData)
						setBarcodeIsScanned(true)
						setIsModalScanBarcodeVisible(false)
					}
					break;
				case 500:
					current.showToast('error', "Connection not available")
					setIsModalScanBarcodeVisible(false)
					break;
				default:
					current.showToast('error', "Connection not available")
					setIsModalScanBarcodeVisible(false)
					break;
			}
		})
	}


	const handleScanbarcode = () => {
		setIsModalScanBarcodeVisible(true)
	}

	const handleTakePhoto = () => {
		setIsModalTakePhotoVisible(true)
	}

	return (
		<View style={{ flex: 1 }}>
			<ScrollView
				onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}>
				<View style={GlobalStyle.formHeaderContentContainer}>
					{submited ?
						<CustomInputComponent
							disabled
							onEndEditing={() => onScanBarcode(tempReceiverBarcode)}
							label='Batch Number'
							value={tempReceiverBarcode}
							placeholder='Input batch number'
							onChangeText={setTempReceiverBarcode}

						/>
						:
						<CustomInputComponent
							onEndEditing={() => onScanBarcode(tempReceiverBarcode)}
							label='Batch Number'
							value={tempReceiverBarcode}
							placeholder='Input batch number'
							onChangeText={setTempReceiverBarcode}

						/>
					}
					<View style={GlobalStyle.detailContainer}>
						<View style={GlobalStyle.detailInputContainerWithoutBorder}>
							{tempReceiverBarcode == "" ?
								<CustomButton
									customColor={Colors.BLUE_LIGHT}
									onPress={() => handleScanbarcode()}
									label='Scan QR Code'
								/>
								:
								<CustomButton
									disabled
									customColor={Colors.BLUE_LIGHT}
									onPress={() => handleScanbarcode()}
									label='Scan QR Code'
								/>
							}
						</View>
					</View>
					<CustomInputComponent
						disabled
						label='Material Code'
						value={materialCode}
					/>
					<CustomInputComponent
						disabled
						label='Material Name'
						value={materialName}
					/>
					<CustomInputComponent
						disabled
						label='Storage Location'
						value={storageLocation}
					/>
					{submited ?
						<>
							<CustomInputComponent
								disabled
								label='Quantity'
								suffix={UOM}
								value={materialQuantity}
								onChangeText={inputValidation(setMaterialQuantity, 'number')}
								keyboardType='numeric'
							/>
							<View style={GlobalStyle.formContentContainer}>
								<CustomInputComponent
									disabled
									isVertical
									label='Notes'
									isLabelBold
									value={catatan}
									placeholder='Notes'
									onChangeText={setCatatan}
								/>
							</View>
						</>
						:
						<>
							{(barcodeIsScanned && tempReceiverBarcode != "") &&
								<CustomInputComponent
									label='Quantity'
									suffix={UOM}
									value={materialQuantity}
									onChangeText={inputValidation(setMaterialQuantity, 'number')}
									keyboardType='numeric'
								/>
							}
							{(barcodeIsScanned && tempReceiverBarcode != "") &&
								<View style={GlobalStyle.formContentContainer}>
									<CustomInputComponent
										isVertical
										label='Notes (Optional)'
										isLabelBold
										value={catatan}
										placeholder='Notes'
										onChangeText={setCatatan}
									/>
								</View>
							}
						</>
					}
				</View>

				<View style={GlobalStyle.formButtonContainer}>
					{barcodeIsScanned && tempReceiverBarcode != "" &&
						<CustomButton
							customColor={Colors.GREEN_LIGHT}
							onPress={() => handleTakePhoto()}
							label='Take Photo'
						/>
					}
					{tempReceiverIDPhoto ? (
						<View style={styles.tempImageStyle}>
							<Image resizeMethod="resize" resizeMode='cover' source={{ uri: tempReceiverIDPhoto }} style={{ height: '100%', width: '100%' }} />
						</View>
					) : null}
				</View>
				<View style={[GlobalStyle.formButtonContainer, { flexDirection: "row" }]}>
					{barcodeIsScanned && tempReceiverBarcode != "" &&
						<CustomButton
							onPress={handleSubmitMaterial}
							isLoading={isLoading}
							label='Save'
						/>
					}
					<View style={{ width: 10 }}></View>
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						label='Back'
					/>
				</View>
			</ScrollView>
			<CustomModalCamera
				onPictureTaken={onPictureTaken}
				onCancel={() => setIsModalTakePhotoVisible(false)}
				isModalVisible={isModalTakePhotoVisible}
			/>
			<CustomModalScanBarcode
				onScanBarcode={onScanBarcode}
				onCancel={() => setIsModalScanBarcodeVisible(false)}
				isModalVisible={isModalScanBarcodeVisible}
			/>
			<CustomModalConfirm
				title='Save Material'
				body='Are you sure want to save material? Make sure your data is correct.'
				onConfirm={onSubmitMaterial}
				onCancel={() => setIsModalConfirmVisible(false)}
				isModalVisible={isModalConfirmVisible}
			/>
			<CustomToast ref={toastRef} />
		</View >
	)
}

export default DetailMaterialStockOpname2

const styles = StyleSheet.create({
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
})