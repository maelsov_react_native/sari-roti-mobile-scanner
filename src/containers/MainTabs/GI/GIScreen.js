import React, { useState, useEffect, useRef } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Animated, ScrollView, FlatList, RefreshControl, Image } from 'react-native'
import { Actions } from 'react-native-router-flux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

import { Colors, Fonts, Icons, Illustrations, Metrics, StorageKeys } from '../../../globals/GlobalConfig'
import GlobalStyle from '../../../globals/GlobalStyle';
import { wait } from '../../../globals/GlobalFunction';
import { modelTransactions } from '../../../models/Transaction';

import CustomToast from '../../../components/CustomToast';
import CustomCalendar from '../../../components/CustomCalendar';

const CardDisplay = (props) => {
	const { data } = props
	const {
		TR_PO_HEADER_NUMBER,
		MA_VENDOR_NAME,
		TR_PO_HEADER_SUP_PLANT,
		TR_PO_HEADER_TYPE,
		TR_PO_HEADER_VENDOR
	} = data

	const openDetailGI = () => {
		AsyncStorage.setItem(StorageKeys.PO_NUMBER_GI, JSON.stringify(TR_PO_HEADER_NUMBER), () => {
			Actions.detailGI({ poNumber: TR_PO_HEADER_NUMBER })
		})
	}
	return (
		<TouchableOpacity style={styles.cardContainer} onPress={() => openDetailGI()}>
			<View style={{ flex: 3 }}>
				<Text style={styles.cardTitleText}>PO Number</Text>
				<Text style={styles.cardTitleText}>Vendor Name</Text>
				<Text style={styles.cardTitleText}>Vendor Code</Text>
				<Text style={styles.cardTitleText}>Supplying Plant</Text>
				<Text style={styles.cardTitleText}>Header Type</Text>
			</View>
			<View style={{ flex: 2, alignItems: "flex-end" }}>
				<Text numberOfLines={1} style={styles.cardDescriptionText}>{TR_PO_HEADER_NUMBER}</Text>
				<Text numberOfLines={1} style={styles.cardDescriptionText}>{MA_VENDOR_NAME}</Text>
				<Text numberOfLines={1} style={styles.cardDescriptionText}>{TR_PO_HEADER_VENDOR}</Text>
				<Text numberOfLines={1} style={styles.cardDescriptionText}>{TR_PO_HEADER_SUP_PLANT}</Text>
				<Text numberOfLines={1} style={styles.cardDescriptionText}>{TR_PO_HEADER_TYPE}</Text>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const GIScreen = (props) => {
	const { lastUpdate } = props
	const [isLoading, setIsLoading] = useState(false)
	const [isStartDate, setIsStartDate] = useState(false)
	const [incorrectDate, setIncorrectDate] = useState(null)
	const [incorrectMessage, setIncorrectMessage] = useState('')
	const [isDateModalVisible, setIsDateModalVisible] = useState(false)
	const [dataGI, setDataGI] = useState([])
	const [filteredGI, setFilteredGI] = useState([])
	const toastRef = useRef(null);
	const [search, setSearch] = useState('')
	const [startDate, setStartDate] = useState(moment(new Date).startOf('month').format('YYYY-MM-DD'))
	const [endDate, setEndDate] = useState(moment(new Date).format('YYYY-MM-DD'))

	useEffect(() => {
		const keys = [
			StorageKeys.PO_NUMBER_GI,
			StorageKeys.GI_DATA_SUBMIT
		]
		AsyncStorage.multiRemove(keys)
		initialLoad()
	}, [lastUpdate])

	const initialLoad = (data, data2) => {
		const { current } = toastRef
		setIsLoading(true)
		wait(2000).then(() => {
			setIsLoading(true)
			let params_data = `start_date=${data != null ? data : startDate}&end_date=${data2 != null ? data2 : endDate}`
			modelTransactions.getGIPOView(params_data, res => {
				const { status, result } = res
				console.log("GI PO VIEW", result)
				console.log(res)
				switch (status) {
					case 200:
						setDataGI(result.data)
						setFilteredGI(result.data)
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const clearGI = () => {
		setSearch("")
		setFilteredGI(dataGI)
	}

	const onGISearch = value => {
		let filteredList = []
		setSearch(value)
		for (let i = 0; i < dataGI.length; i++) {
			if (String(dataGI[i].TR_PO_HEADER_NUMBER).includes(String(value).toUpperCase()) ||
				String(dataGI[i].TR_PO_HEADER_VENDOR).includes(String(value).toUpperCase()) ||
				String(dataGI[i].MA_VENDOR_NAME).includes(String(value).toUpperCase())) filteredList.push(dataGI[i])
			if (i === dataGI.length - 1) setFilteredGI(filteredList)
		}
	}

	const funcGetDate = (res) => {
		if (!isStartDate) {
			if (res.dateString > endDate) {
				setStartDate(moment().startOf('month').format('YYYY-MM-DD'))
				setEndDate(moment(new Date).format('YYYY-MM-DD'))
				setIsStartDate(false)
				setIncorrectDate(new Date)
				setIncorrectMessage("Start date must be smaller than end date")
			} else {
				setStartDate(res.dateString)
				setIsStartDate(true)
			}
		}
		else {
			if (startDate > res.dateString) {
				setStartDate(moment().startOf('month').format('YYYY-MM-DD'))
				setEndDate(moment(new Date).format('YYYY-MM-DD'))
				setIsStartDate(false)
				setIncorrectDate(new Date)
				setIncorrectMessage("Start date must be smaller than end date")
			}
			else {
				setEndDate(res.dateString)
				setIsStartDate(false)
				setIsDateModalVisible(false)
				initialLoad(startDate, res.dateString)
			}
		}
	}

	return (
		<View style={{ flex: 1 }}>
			<View style={{ paddingTop: Metrics.SAFE_AREA, width: '100%', backgroundColor: Colors.WHITE, elevation: 2 }}>
				<View style={{ width: '100%', paddingHorizontal: Metrics.SAFE_AREA, height: 60, paddingBottom: 10, justifyContent: 'center', marginLeft: -2, marginBottom: 10 }}>
					<View style={{ flex: 1, flexDirection: 'row', borderRadius: 50, backgroundColor: Colors.GRAY_LIGHT, overflow: 'hidden', paddingRight: 10 }}>
						<View style={{ height: '100%', justifyContent: 'center', marginHorizontal: 10 }}>
							<Ionicons name="search-outline" size={18} color="black" />
						</View>
						<TextInput
							placeholder='Cari Nomor Purchase Order'
							placeholderTextColor={Colors.GRAY}
							value={search}
							style={{ flex: 1, fontSize: 14, fontFamily: Fonts.ROBOTO_REGULAR, color: Colors.BLACK }}
							onChangeText={onGISearch}
							returnKeyType='search'
						/>
						{search !== '' ?
							<TouchableOpacity onPress={clearGI}>
								<View style={{ height: '100%', justifyContent: 'center', marginLeft: 10 }}>
									<Ionicons name="close-circle-outline" size={24} color="black" />
								</View>
							</TouchableOpacity> : null}
					</View>
				</View>
			</View>
			<TouchableOpacity
				onPress={() => setIsDateModalVisible(true)}
				style={styles.dateStyle}>
				<Text style={{ fontSize: 14, fontFamily: Fonts.ROBOTO_REGULAR, letterSpacing: 0.5, lineHeight: 24, color: Colors.SemiBlackColor }}>Select Start Date</Text>
				<View style={{ alignItems: 'center', flexDirection: 'row' }}>
					<Ionicons name="calendar-outline" size={22} color="black" />
					<Text style={{ fontSize: 14, fontFamily: Fonts.ROBOTO_REGULAR, letterSpacing: 0.5, lineHeight: 24, color: Colors.SemiBlackColor }}> {moment(startDate).format('DD MMMM')} - {moment(endDate).format('DD MMMM')}</Text>
				</View>
			</TouchableOpacity>
			<FlatList
				refreshControl={(
					<RefreshControl
						refreshing={isLoading}
						onRefresh={initialLoad}
					/>
				)}
				data={filteredGI}
				extraData={filteredGI}
				keyExtractor={item => String(item.TR_PO_HEADER_ID)}
				contentContainerStyle={GlobalStyle.viewContainer}
				// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
				onEndReachedThreshold={0.1}
				ListEmptyComponent={(
					<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
							<View style={styles.illustContainerImage}>
								<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
								<View style={styles.titleTextContainer}>
									<Text style={styles.illustTextDesc}>Good Issue is empty</Text>
								</View>
							</View>
					</View>
				)}
				renderItem={({ item, index }) => (
					<CardDisplay
						data={item} />
				)}
			// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
			// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
			/>
			<CustomCalendar
				message={incorrectMessage}
				lastUpdate={incorrectDate}
				isStartDate={isStartDate}
				onBackdropPress={() => setIsDateModalVisible(false)}
				currentDate={startDate}
				endDate={endDate}
				funcGetDate={funcGetDate}
				isDateModalVisible={isDateModalVisible}
			/>
			<CustomToast ref={toastRef} />
		</View>
	)
}

const styles = StyleSheet.create({
	dateStyle: {
		backgroundColor: Colors.WhiteColor,
		height: 60,
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		borderBottomWidth: 3,
		borderBottomColor: Colors.GRAY_LIGHT,
	},
	cardContainer: {
		flexDirection: "row",
		marginBottom: 20,
		marginHorizontal: 5,
		paddingVertical: 10,
		paddingHorizontal: 15,
		borderRadius: 5,
		backgroundColor: Colors.WHITE,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.50,
		shadowRadius: 1.45,
		elevation: 2,
	},
	cardTitleText: {
		fontSize: 14,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: 'bold'
	},
	cardDescriptionText: {
		fontSize: 12,
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontWeight: 'bold',
		marginBottom: 4
	},
	illustContainerImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    illustStyle: {
        width: 200,
        height: 200,
        resizeMode: 'cover',
    },
    illustTextDesc: {
        fontFamily: Fonts.ROBOTO_REGULAR,
        fontSize: 14,
        textAlign: 'center',
        marginTop: 20,
        color: Colors.BLACK,
    },
})

export default GIScreen