import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, View, Text, StyleSheet, TouchableOpacity, Image, FlatList, RefreshControl } from 'react-native'
import { Actions } from 'react-native-router-flux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

import { Colors, Fonts, Icons, Illustrations, Metrics } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { getGIDataSubmit, getPoNumberGI, wait } from '../../../../globals/GlobalFunction';
import { modelTransactions } from '../../../../models/Transaction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomModalCamera from '../../../../components/CustomModalCamera';
import CustomButton from '../../../../components/CustomButton';
import CustomToast from '../../../../components/CustomToast';
import CustomModalConfirm from '../../../../components/CustomModalConfirm';

const CardDisplay = (props) => {
	const { data, materialData, subPlant, supPlant } = props
	const {
		TR_GI_SAPDETAIL_ID,
		TR_GI_SAPDETAIL_BASE_UOM,
		TR_GI_SAPDETAIL_MATERIAL_NAME,
		TR_GI_SAPDETAIL_GI_QTY,
		TR_GI_SAPDETAIL_SLOC,
		TR_GI_SAPDETAIL_MATERIAL_CODE,
		TR_GI_SAPDETAIL_SAP_BATCH,
		TR_GI_SAPDETAIL_GI_UOM,
	} = data

	const isChecked = materialData.findIndex(e => e.SAPDetailID == TR_GI_SAPDETAIL_ID) > -1
	const found = materialData.find(e => e.SAPDetailID == TR_GI_SAPDETAIL_ID)
	const goToDetailMaterial = () => {
		if (isChecked) {
			Actions.detailMaterialGI({ SAPDetailID: TR_GI_SAPDETAIL_ID, materialCode: TR_GI_SAPDETAIL_MATERIAL_CODE, materialName: TR_GI_SAPDETAIL_MATERIAL_NAME, storageLocation: TR_GI_SAPDETAIL_SLOC, totalQTY: TR_GI_SAPDETAIL_GI_QTY, foto: found.photo, foto2: found.photoTemp, note: found.note, qty: found.qty, uom: found.batch, submited: true, batchNumber: found.batchNumber, plant: supPlant, SAPBatch: TR_GI_SAPDETAIL_SAP_BATCH })
		}
		else {
			Actions.detailMaterialGI({ SAPDetailID: TR_GI_SAPDETAIL_ID, materialCode: TR_GI_SAPDETAIL_MATERIAL_CODE, materialName: TR_GI_SAPDETAIL_MATERIAL_NAME, storageLocation: TR_GI_SAPDETAIL_SLOC, totalQTY: TR_GI_SAPDETAIL_GI_QTY, uom: TR_GI_SAPDETAIL_BASE_UOM, plant: supPlant, SAPBatch: TR_GI_SAPDETAIL_SAP_BATCH })
		}
	}
	return (
		<TouchableOpacity
			style={{ flexDirection: "row", paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: Colors.GRAY_DARK, marginBottom: 20 }}
			onPress={() => goToDetailMaterial()}>
			<View style={{ flex: 4 }}>
				<CustomInputComponent
					disabled
					label='Material'
					value={TR_GI_SAPDETAIL_MATERIAL_NAME}
				/>
				<CustomInputComponent
					disabledTextInput
					suffix={TR_GI_SAPDETAIL_BASE_UOM}
					label='Quantity'
					value={TR_GI_SAPDETAIL_GI_QTY}
				/>
				<CustomInputComponent
					disabled
					label='Sub Plant'
					value={subPlant}
				/>
				<CustomInputComponent
					disabled
					label='Sloc'
					value={TR_GI_SAPDETAIL_SLOC}
				/>
				{isChecked &&
					<CustomInputComponent
						disabled
						label='Issued Quantity'
						value={found.qty}
					/>
				}
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				{isChecked && (
					<FontAwesome
						name='check'
						color={Colors.GREEN}
						size={16}
						style={{ position: "absolute", top: 5, right: 5, height: 20, width: 20 }}
					/>
				)
				}
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const DetailGIScreen = (props) => {
	const { lastUpdate } = props
	const [scrollPosition, setScrollPosition] = useState(0)
	const [tempReceiverIDPhoto, setTempReceiverIDPhoto] = useState('')
	const [fotoBase64AbsensiList, setFotoBase64AbsensiList] = useState('')

	const [materialList, setMaterialList] = useState([])
	const [submitedMaterialList, setSubmitedMaterialList] = useState([])
	const [filteredMaterialList, setFilteredMaterialList] = useState([])
	const [filteredMaterialListIndex, setFilteredMaterialListIndex] = useState("5")

	const [SAPHeaderID, setSAPHeaderID] = useState('')
	const [poNumber, setPoNumber] = useState('')
	const [MVTCode, setMVTCode] = useState('')
	const [catatan, setCatatan] = useState('')
	const [BOL, setBOL] = useState('')
	const [postingDate, setPostingDate] = useState('')
	const [createdDate, setCreatedDate] = useState('')
	const [plant, setPlant] = useState('')
	const [vendorName, setVendorName] = useState('')
	const [subPlant, setSubPlant] = useState('')
	const [mode, setMode] = useState('')

	const [isLoading, setIsLoading] = useState(false)
	const [isModalConfirmVisible, setIsModalConfirmVisible] = useState(false)
	const [isModalTakePhotoVisible, setIsModalTakePhotoVisible] = useState(false)
	const [isShowDatePicker, setIsShowDatePicker] = useState(false)

	const toastRef = useRef(null);


	useEffect(() => {
		if (lastUpdate) {
			getGIDataSubmit().then(result => {
				if (result != null) {
					setSubmitedMaterialList(result)
					console.log(result)
				}
			})
			getPoNumberGI().then(result => {
				setPoNumber(result)
				initialLoad(result)
			})
		}
	}, [lastUpdate])

	const initialLoad = (data) => {
		setIsLoading(true)
		wait(2000).then(() => {
			let params_data = `gi_po_number=${data}`
			modelTransactions.getGIPODetail(params_data, res => {
				const { status, result } = res
				console.log("DATA", result)
				switch (status) {
					case 200:
						let params_data = `TR_GI_SAPHEADER_ID=${result.data.gi_data[0].TR_GI_SAPHEADER_ID}`
						modelTransactions.getGIMaterialDetail(params_data, res => {
							const { status, result } = res
							console.log(res)
							console.log("DATA", result.data.gi_detail_data)
							switch (status) {
								case 200:
									setMVTCode(result.data.gi_header_data.TR_GI_SAPHEADER_MVT_CODE)
									setMaterialList(result.data.gi_detail_data)
									if (result.data.gi_detail_data.length > 5) {
										setFilteredMaterialList(result.data.gi_detail_data.slice(0, filteredMaterialListIndex))
									}
									else {
										setFilteredMaterialList(result.data.gi_detail_data)
									}
									break;
								case 500:
									current.showToast('error', "Connection not available")
									break;
								default:
									current.showToast('error', "Connection not available")
									break;
							}
						})
						setCreatedDate(moment(result.data.header_data.TR_PO_HEADER_CREATED_TIMESTAMP).format('YYYY-MM-DD'))
						setVendorName(result.data.header_data.TR_PO_HEADER_VENDOR)
						setPlant(result.data.header_data.TR_PO_HEADER_SUP_PLANT)
						setSubPlant(result.data.header_data.TR_PO_HEADER_SUP_PLANT)
						setSAPHeaderID(result.data.gi_data[0].TR_GI_SAPHEADER_ID)
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const handleTakePhoto = () => {
		setIsModalTakePhotoVisible(true)
	}

	const onPictureTaken = (cameraData) => {
		const {
			uri,
			base64
		} = cameraData

		setFotoBase64AbsensiList(base64)
		setTempReceiverIDPhoto(uri)
		setIsModalTakePhotoVisible(false)
	}

	const handleSubmitGI = () => {
		const { current } = toastRef
		if (postingDate == '') {
			current.showToast('warning', "Mohon input tanggal posting!")
		}
		else if (tempReceiverIDPhoto === '') {
			current.showToast('warning', "Mohon ambil foto!")
		}
		else if (catatan === '') {
			current.showToast('warning', "Mohon input catatan!")
		}
		else {
			const jumlahData = materialList.reduce((acc, cur) => {
				const found = submitedMaterialList.findIndex(e => e.SAPDetailID == cur.TR_GI_SAPDETAIL_ID) > -1
				if (found) return acc + 1
				else acc
			}, 0)

			const goodData = jumlahData == materialList.length
			console.log(jumlahData)
			console.log(materialList.length)
			console.log(goodData)
			if (goodData) {
				setIsModalConfirmVisible(true)
			}
			else {
				current.showToast('warning', "Mohon submit material lainnya!")
			}
		}
	}

	const onSubmitGI = () => {
		// getUserData().then(result => {
		const { current } = toastRef
		setIsLoading(true)
		const post_data = new FormData();
		post_data.append('TR_GI_SAPHEADER_ID', SAPHeaderID)
		post_data.append('TR_GI_SAPHEADER_PSTG_DATE', moment(postingDate).format('YYYY-MM-DD'))
		post_data.append('TR_GI_SAPHEADER_TXT', catatan)
		post_data.append('TR_GI_SAPHEADER_BOL', BOL)
		post_data.append('GI_PHOTO', fotoBase64AbsensiList)
		for (let index = 0; index < submitedMaterialList.length; index++) {
			const element = submitedMaterialList[index];
			post_data.append('gi_materials[' + index + '][TR_GI_SAPDETAIL_ID]', element.SAPDetailID)
			post_data.append('gi_materials[' + index + '][TR_GI_SAPDETAIL_MOBILE_QTY]', element.qty)
			post_data.append('gi_materials[' + index + '][TR_GI_SAPDETAIL_MOBILE_UOM]', element.uom)
			post_data.append('gi_materials[' + index + '][TR_GI_SAPDETAIL_NOTES]', element.note)
			post_data.append('gi_materials[' + index + '][materialPhoto]', element.photo)
		}
		console.log(post_data)
		modelTransactions.GISubmit(post_data, res => {
			const { status, result } = res
			console.log(res)
			switch (status) {
				case 200:
					setIsModalConfirmVisible(false)
					setIsLoading(true)
					wait(2000).then(() => {
						current.showToast('success', "Good Issue has been submitted successfully!")
						wait(2000).then(() => {
							setIsLoading(false)
							Actions.pop()
						})
					})
					break;
				case 400:
					setIsLoading(false)
					setIsModalConfirmVisible(false)
					current.showToast('warning', "Data already Submitted, Transaction Cancelled")
					break;
				default:
					setIsLoading(false)
					setIsModalConfirmVisible(false)
					current.showToast('warning', `*${result.message}*`)
					break;
			}
		})
		// })
	}

	const loadOtherIndex = () => {
		setIsLoading(true)
		const index = parseInt(filteredMaterialListIndex) + 5
		if (index < materialList.length) {
			setFilteredMaterialListIndex(index)
			setFilteredMaterialList(materialList.slice(0, index))
			setIsLoading(false)
		}
		else if (index >= materialList.length) {
			setFilteredMaterialListIndex(materialList.length)
			setFilteredMaterialList(materialList.slice(0, materialList.length))
			setIsLoading(false)
		}
	}

	const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
		const paddingToBottom = 20;
		return layoutMeasurement.height + contentOffset.y >=
			contentSize.height - paddingToBottom;
	};

	const changeDate = () => {
		setIsShowDatePicker(true)
	}

	const onChangeDate = (event, selectedDate) => {
		const currentDate = selectedDate || date;
		console.log(currentDate)
		setIsShowDatePicker(false);
		setPostingDate(currentDate);
	};


	return (
		<View style={{ flex: 1 }}>
			<ScrollView>
				<View style={GlobalStyle.formHeaderContentContainer}>
					<CustomInputComponent
						disabled
						label='PO Number'
						value={poNumber}
					/>
					<CustomInputComponent
						disabled
						label='Vendor Name'
						value={vendorName}
					/>
					<CustomInputComponent
						disabled
						label='Movement Type'
						value={MVTCode}
					/>
					<CustomInputComponent
						disabled
						label='Supplying Plant'
						value={plant}
					/>
					{/* <CustomInputComponent
								disabled
								label='GL Account'
								value="-"
							/>
							<CustomInputComponent
								disabled
								label='Cost Center'
								value="-"
							/> */}
					<CustomInputComponent
						disabled
						label='Created Date'
						value={createdDate}
					/>
					<CustomInputComponent
						isDate
						onDatePress={() => changeDate()}
						label='Posting Date'
						value={postingDate}
					/>
					{isShowDatePicker && (
						<DateTimePicker
							testID="dateTimePicker"
							value={new Date()}
							mode={mode}
							is24Hour={true}
							display="default"
							onChange={onChangeDate}
						/>)}
				</View>

				<View style={GlobalStyle.formContentContainer}>
					<Text style={GlobalStyle.formHeaderTitleText}>List of Material ({materialList.length})</Text>
					<View style={{ marginTop: 10 }}>
						<FlatList
							nestedScrollEnabled={true}
							style={{ maxHeight: Metrics.SCREEN_WIDTH * 2 }}
							onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}
							onScrollEndDrag={({ nativeEvent }) => {
								if (isCloseToBottom(nativeEvent) && materialList.length > filteredMaterialListIndex && isLoading === false) {
									loadOtherIndex()
								}
							}}
							scrollEventThrottle={100}
							refreshControl={(
								<RefreshControl
									refreshing={isLoading}
									onRefresh={initialLoad}
								/>
							)}
							data={filteredMaterialList}
							extraData={filteredMaterialList}
							keyExtractor={item => String(item.TR_GI_SAPDETAIL_ID)}
							contentContainerStyle={GlobalStyle.viewContainer}
							// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
							onEndReachedThreshold={0.1}
							ListEmptyComponent={(
								<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
									<View style={styles.illustContainerImage}>
										<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
										<View style={styles.titleTextContainer}>
											<Text style={styles.illustTextDesc}>Material is empty</Text>
										</View>
									</View>
								</View>
							)}
							renderItem={({ item, index }) => (
								<CardDisplay
									subPlant={subPlant}
									supPlant={plant}
									SAPHeaderID={SAPHeaderID}
									materialData={submitedMaterialList}
									data={item} />
							)}
						// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
						// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
						/>
					</View>
				</View>

				<View style={GlobalStyle.formContentContainer}>
					<View style={[GlobalStyle.formButtonContainer, { paddingHorizontal: 0 }]}>
						<CustomButton
							customColor={Colors.GREEN_LIGHT}
							onPress={() => handleTakePhoto()}
							label='Take Photo'
						/>
						{tempReceiverIDPhoto ? (
							<View style={styles.tempImageStyle}>
								<Image resizeMethod="resize" resizeMode='cover' source={{ uri: tempReceiverIDPhoto }} style={{ height: '100%', width: '100%' }} />
							</View>
						) : null}
					</View>
				</View>

				<View style={GlobalStyle.formContentContainer}>
					<CustomInputComponent
						isVertical
						label='Notes'
						isLabelBold
						value={catatan}
						placeholder='Notes'
						onChangeText={setCatatan}
					/>
				</View>
				<View style={GlobalStyle.formContentContainer}>
					<CustomInputComponent
						isVertical
						label='BOL (Optional)'
						isLabelBold
						value={BOL}
						placeholder='Bill Of Landing'
						onChangeText={setBOL}
					/>
				</View>
				<View style={[GlobalStyle.formButtonContainer, { flexDirection: "row" }]}>
					<CustomButton
						onPress={handleSubmitGI}
						isLoading={isLoading}
						label='Submit'
					/>
					<View style={{ width: 10 }}></View>
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						isLoading={isLoading}
						label='Back'
					/>
				</View>
			</ScrollView>
			<CustomModalConfirm
				title='Submit Good Issue'
				body='Are you sure want to submit Good Issue? Make sure your data are correct.'
				onConfirm={onSubmitGI}
				onCancel={() => setIsModalConfirmVisible(false)}
				isModalVisible={isModalConfirmVisible}
			/>
			<CustomModalCamera
				onPictureTaken={onPictureTaken}
				onCancel={() => setIsModalTakePhotoVisible(false)}
				isModalVisible={isModalTakePhotoVisible}
			/>
			<CustomToast ref={toastRef} />
		</View >
	)
}

export default DetailGIScreen

const styles = StyleSheet.create({
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	illustContainerImage: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	illustStyle: {
		width: 200,
		height: 200,
		resizeMode: 'cover',
	},
	illustTextDesc: {
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontSize: 14,
		textAlign: 'center',
		marginTop: 20,
		color: Colors.BLACK,
	},
})