import React, { useEffect, useRef, useState } from 'react'
import {
	StyleSheet,
	Text,
	View,
	Image,
	ActivityIndicator,
	TouchableNativeFeedback
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import AsyncStorage from '@react-native-community/async-storage';

import { Colors, Fonts, Icons, Metrics, StorageKeys } from '../../../globals/GlobalConfig'
import GlobalStyle from '../../../globals/GlobalStyle'
import { getUserData, wait } from '../../../globals/GlobalFunction'
import { modelAuthentication } from '../../../models/Authentication';

import CustomModalConfirm from '../../../components/CustomModalConfirm'
import CustomToast from '../../../components/CustomToast'

const AkunScreen = () => {
	const [userLocation, setUserLocation] = useState('')
	const [userLocationName, setUserLocationName] = useState('')
	const [userId, setUserId] = useState('')
	const [userName, setUserName] = useState('')
	const [userLoginName, setUserLoginName] = useState('')

	const [isEdited, setIsEdited] = useState(false)
	const [isLoading, setIsLoading] = useState(false)
	const [isModalConfirmVisible, setIsModalConfirmVisible] = useState(false)

	const toastRef = useRef(null);

	useEffect(() => {
		getUserData().then(result => {
			setUserId(result.user_id)
			setUserLocation(result.user_plant)
			// setUserLocationName("Storage A, Bekasi Barat")
			setUserLocationName("")
			setUserName(result.user_initial_name)
		})
		setUserLoginName("vincent_alexander")
	}, [])

	const handleLogout = () => {
		const { current } = toastRef
		setIsModalConfirmVisible(false)
		setIsLoading(true)
		modelAuthentication.logout(userId, async res => {
			const { status, result } = res
			console.log(res)
			switch (status) {
				case 200:
					const keys = [
						StorageKeys.CUSTOMER_TOKEN,
						StorageKeys.USER_DATA,
					]
					AsyncStorage.multiRemove(keys)
						wait(2000).then(() => {
							current.showToast('success', "Logout success")
							wait(2000).then(() => {
								setIsLoading(false)
								Actions.login()
							})
						})
					break;
				case 400:
					setIsLoading(false)
					// current.showToast('warning', "Username atau Kata Sandi salah!")
					break;
				default:
					setIsLoading(false)
					current.showToast('warning', `*${result.message}* Logout Gagal`)
					break;
			}
		})
			.catch(err => {
				console.log(err)
				setIsLoading(false)
				current.showToast('error', DefaultErrorMessage)
			})
	}

	return (
		<View style={{ flex: 1 }}>
			<View style={{ flex: 1 }}>
				<View style={styles.accountInfoContainer}>
					<Text style={styles.accountInfoInstruction}>[{userLocation}] {userLocationName}</Text>
					<Text style={styles.accountInfoTitle}>{userName}</Text>
				</View>
				<TouchableNativeFeedback>
					<View style={styles.menuContainer}>
						<View style={{ flex: 1, justifyContent: 'center' }}>
							<Text style={styles.menuTitle}>Change Password</Text>
						</View>
						<View style={{ height: '100%', justifyContent: "center" }}>
							<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
						</View>
					</View>
				</TouchableNativeFeedback>
			</View>
			<View style={{ paddingVertical: 20, paddingHorizontal: Metrics.SAFE_AREA }}>
				<Text style={[styles.menuTitle, { marginBottom: 20, textAlign: "center" }]}>v1.0</Text>
				<TouchableNativeFeedback onPress={() => setIsModalConfirmVisible(true)}>
					<View style={styles.buttonContaienr}>
						{isLoading ?
							<ActivityIndicator color="#fff" size='small' />
							:
							<Text style={styles.buttonText}>Logout</Text>
						}
					</View>
				</TouchableNativeFeedback>
			</View>
			<CustomModalConfirm
				title='Logout'
				body='Are you sure want to logout?'
				onConfirm={handleLogout}
				onCancel={() => setIsModalConfirmVisible(false)}
				isModalVisible={isModalConfirmVisible}
			/>
			<CustomToast ref={toastRef} />
		</View>
	)
}

export default AkunScreen

const styles = StyleSheet.create({
	menuContainer: {
		height: 60,
		width: '100%',
		flexDirection: 'row',
		paddingLeft: Metrics.SAFE_AREA,
		paddingRight: Metrics.SAFE_AREA - 8,
		backgroundColor: Colors.WHITE,
		marginBottom: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	accountInfoContainer: {
		height: 60,
		marginBottom: 20,
		width: '100%',
		justifyContent: 'center',
		paddingHorizontal: 20,
		backgroundColor: Colors.WHITE,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	accountInfoInstruction: {
		...GlobalStyle.textFontSize12,
		color: Colors.DARK,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: "bold"
	},
	accountInfoTitle: {
		...GlobalStyle.textFontSize16,
		color: Colors.DARK,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: "bold"
	},
	menuTitle: {
		...GlobalStyle.textFontSize16,
		color: Colors.DARK,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: "bold"
	},
	menuDesc: {
		...GlobalStyle.textFontSize10,
		color: Colors.DARK,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: "bold"
	},
	buttonContaienr: {
		height: 40,
		width: '100%',
		borderRadius: 25,
		borderWidth: 1,
		borderColor: Colors.RED,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonText: {
		...GlobalStyle.textFontSize14,
		color: Colors.RED,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: "bold"
	}
})