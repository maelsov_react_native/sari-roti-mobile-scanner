import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, View, Text, StyleSheet, FlatList, Image, RefreshControl } from 'react-native'
import { Actions } from 'react-native-router-flux';

import { Colors, Fonts, Icons, Illustrations, Metrics } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { wait } from '../../../../globals/GlobalFunction';
import { modelTransactions } from '../../../../models/Transaction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomButton from '../../../../components/CustomButton';

const CardDisplay = (props) => {
	const { data } = props
	const {
		TR_GR_DETAIL_MATERIAL_NAME,
		TR_GR_DETAIL_MATERIAL_CODE,
		TR_GR_DETAIL_QTY,
		TR_GR_DETAIL_UNLOADING_PLANT,
		TR_GR_DETAIL_UOM,
		TR_PID_DETAIL_PHOTO
	} = data


	return (
		<>
			<View style={{ borderBottomWidth: 1, borderBottomColor: Colors.GRAY_DARK, paddingBottom: 20 }}>
				<View style={{ flexDirection: "row" }}>
					<View style={{ flex: 4 }}>
						<CustomInputComponent
							disabled
							label='Material'
							value={TR_GR_DETAIL_MATERIAL_NAME}
						/>
						<CustomInputComponent
							disabled
							label='Material Code'
							value={TR_GR_DETAIL_MATERIAL_CODE}
						/>
						<CustomInputComponent
							disabledTextInput
							suffix={TR_GR_DETAIL_UOM}
							label='Quantity'
							value={TR_GR_DETAIL_QTY}
						/>
						<CustomInputComponent
							disabled
							label='Plant'
							value={TR_GR_DETAIL_UNLOADING_PLANT}
						/>
					</View>
					<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
						<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
					</View>
				</View>
				{TR_PID_DETAIL_PHOTO != null && TR_PID_DETAIL_PHOTO != "" &&
					<View style={styles.tempImageStyle}>
						<Image resizeMethod="resize" resizeMode='cover' source={{ uri: TR_PID_DETAIL_PHOTO }} style={{ height: '100%', width: '100%' }} />
					</View>
				}
			</View>
		</>
	)
}

const DetailPIDMaterialScreen = (props) => {
	const [scrollPosition, setScrollPosition] = useState(0)
	const [isLoading, setIsLoading] = useState(false)

	const [materialList, setMaterialList] = useState([])
	const [filteredMaterialList, setFilteredMaterialList] = useState([])
	const [filteredMaterialListIndex, setFilteredMaterialListIndex] = useState("5")

	const [isModalConfirmVisible, setIsModalConfirmVisible] = useState(false)
	const toastRef = useRef(null);

	useEffect(() => {
		initialLoad()
	}, [])

	const initialLoad = () => {
		setIsLoading(true)
		wait(2000).then(() => {
			let params_data = `TR_PID_DETAIL_ID=${props.pidDetailID}`
			modelTransactions.getPIDDetailMaterialHistory(params_data, res => {
				const { status, result } = res
				console.log(res)
				console.log("MATERIAL", result.data.detail_material)
				switch (status) {
					case 200:
						setMaterialList(result.data.detail_material)
						if (result.data.detail_material.length > 5) {
							setFilteredMaterialList(result.data.detail_material.slice(0, filteredMaterialListIndex))
						}
						else {
							setFilteredMaterialList(result.data.detail_material)
						}
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const loadOtherIndex = () => {
		setIsLoading(true)
		const index = parseInt(filteredMaterialListIndex) + 5
		if (index < materialList.length) {
			setFilteredMaterialListIndex(index)
			setFilteredMaterialList(materialList.slice(0, index))
			setIsLoading(false)
		}
		else if (index >= materialList.length) {
			setFilteredMaterialListIndex(materialList.length)
			setFilteredMaterialList(materialList.slice(0, materialList.length))
			setIsLoading(false)
		}
	}

	const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
		const paddingToBottom = 20;
		return layoutMeasurement.height + contentOffset.y >=
			contentSize.height - paddingToBottom;
	};

	return (
		<View style={{ flex: 1 }}>
			<ScrollView>
				<View style={GlobalStyle.formContentContainer}>
					<Text style={GlobalStyle.formHeaderTitleText}>List of Material ({materialList.length})</Text>
					<View style={{ marginTop: 10 }}>
						<FlatList
							nestedScrollEnabled={true}
							style={{ maxHeight: Metrics.SCREEN_WIDTH*2 }}
							onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}
							onScrollEndDrag={({ nativeEvent }) => {
								if (isCloseToBottom(nativeEvent) && materialList.length > filteredMaterialListIndex && isLoading === false) {
									loadOtherIndex()
								}
							}}
							scrollEventThrottle={100}
							refreshControl={(
								<RefreshControl
									refreshing={isLoading}
									onRefresh={initialLoad}
								/>
							)}
							data={filteredMaterialList}
							extraData={filteredMaterialList}
							keyExtractor={item => String(item.TR_PID_DETAIL_MATERIAL_ID)}
							contentContainerStyle={GlobalStyle.viewContainer}
							// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
							onEndReachedThreshold={0.1}
							ListEmptyComponent={(
								<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
									<View style={styles.illustContainerImage}>
										<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
										<View style={styles.titleTextContainer}>
											<Text style={styles.illustTextDesc}>Material is empty</Text>
										</View>
									</View>
								</View>
							)}
							renderItem={({ item, index }) => (
								<CardDisplay
									data={item} />
							)}
						// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
						// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
						/>
					</View>
				</View>
				<View style={GlobalStyle.formButtonContainer}>
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						label='Back'
					/>
				</View>
			</ScrollView>
		</View>
	)
}

export default DetailPIDMaterialScreen

const styles = StyleSheet.create({
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	illustContainerImage: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	illustStyle: {
		width: 200,
		height: 200,
		resizeMode: 'cover',
	},
	illustTextDesc: {
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontSize: 14,
		textAlign: 'center',
		marginTop: 20,
		color: Colors.BLACK,
	},
})