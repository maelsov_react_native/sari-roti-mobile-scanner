import React, { useEffect, useRef, useState } from 'react'
import { FlatList, ScrollView, View, Text, StyleSheet, TouchableOpacity, TouchableNativeFeedback, Platform, TextInput, Image, RefreshControl } from 'react-native'
import { Actions } from 'react-native-router-flux';
import Geolocation from 'react-native-geolocation-service';
import Modal from 'react-native-modal';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import Ionicons from "react-native-vector-icons/Ionicons";

import { Colors, Fonts, Icons, Illustrations, Metrics, PropertyColors } from '../../../globals/GlobalConfig';
import GlobalStyle from '../../../globals/GlobalStyle';
import { getCurrentDateTime, getDate, inputValidation, wait } from '../../../globals/GlobalFunction';

import CustomInputComponent from '../../../components/CustomInputComponent'
import CustomModalCamera from '../../../components/CustomModalCamera';
import CustomModalScanBarcode from '../../../components/CustomModalScanBarcode';
import CustomButton from '../../../components/CustomButton';
import CustomToast from '../../../components/CustomToast';
import CustomModalConfirm from '../../../components/CustomModalConfirm';
import CustomGPSIndicator from '../../../components/CustomGPSIndicator';
import CustomImage from '../../../components/CustomImage';
import { modelTransactions } from '../../../models/Transaction';
import CustomCalendar from '../../../components/CustomCalendar';

const CardDisplayGR = (props) => {
	const { data } = props
	const {
		TR_GR_HEADER_ID,
		TR_GR_HEADER_PO_NUMBER,
		TR_GR_HEADER_MVT_CODE,
		TR_GR_HEADER_SAP_DOC,
		TR_GR_HEADER_CREATED_TIMESTAMP
	} = data

	const openDetailGR = () => {
		Actions.detailHistoryGR({ grHeaderID: TR_GR_HEADER_ID })
	}
	return (
		<TouchableOpacity style={styles.cardContainer} onPress={() => openDetailGR()}>
			<View style={{ flex: 3 }}>
				<Text style={styles.cardTitleText}>PO Number</Text>
				<Text style={styles.cardTitleText}>Movement Code</Text>
				<Text style={styles.cardTitleText}>SAP Doc Number</Text>
				<Text style={styles.cardTitleText}>Created Date</Text>
			</View>
			<View style={{ flex: 2, alignItems: "flex-end" }}>
				<Text style={styles.cardDescriptionText}>{TR_GR_HEADER_PO_NUMBER}</Text>
				<Text style={styles.cardDescriptionText}>{TR_GR_HEADER_MVT_CODE}</Text>
				<Text style={styles.cardDescriptionText}>{TR_GR_HEADER_SAP_DOC}</Text>
				<Text style={styles.cardDescriptionText}>{moment(TR_GR_HEADER_CREATED_TIMESTAMP).format('YYYY-MM-DD')}</Text>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const CardDisplayGI = (props) => {
	const { data } = props
	const {
		TR_GI_SAPHEADER_ID,
		TR_GI_SAPHEADER_PO_NUMBER,
		TR_GI_SAPHEADER_MVT_CODE,
		TR_GI_SAPHEADER_SAP_DOC,
		TR_GI_SAPHEADER_CREATED_TIMESTAMP,
	} = data
	return (
		<TouchableOpacity style={styles.cardContainer} onPress={() => Actions.detailHistoryGI({ sapHeaderID: TR_GI_SAPHEADER_ID })}>
			<View style={{ flex: 3 }}>
				<Text style={styles.cardTitleText}>PO Number</Text>
				<Text style={styles.cardTitleText}>Movement Code</Text>
				<Text style={styles.cardTitleText}>SAP Doc Number</Text>
				<Text style={styles.cardTitleText}>Created Date</Text>
			</View>
			<View style={{ flex: 2, alignItems: "flex-end" }}>
				<Text style={styles.cardDescriptionText}>{TR_GI_SAPHEADER_PO_NUMBER}</Text>
				<Text style={styles.cardDescriptionText}>{TR_GI_SAPHEADER_MVT_CODE}</Text>
				<Text style={styles.cardDescriptionText}>{TR_GI_SAPHEADER_SAP_DOC}</Text>
				<Text style={styles.cardDescriptionText}>{moment(TR_GI_SAPHEADER_CREATED_TIMESTAMP).format('YYYY-MM-DD')}</Text>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const CardDisplayTP = (props) => {
	const { data } = props
	const {
		TR_TP_HEADER_ID,
		TR_TP_HEADER_SAP_DOC,
		TR_TP_HEADER_CREATED_BY,
		TR_TP_HEADER_MVT_CODE,
		TR_TP_HEADER_CREATED_TIMESTAMP,
	} = data
	return (
		<TouchableOpacity style={styles.cardContainer} onPress={() => Actions.detailHistoryTP({ headerID: TR_TP_HEADER_ID })}>
			<View style={{ flex: 3 }}>
				<Text style={styles.cardTitleText}>Document ID</Text>
				<Text style={styles.cardTitleText}>Created By</Text>
				<Text style={styles.cardTitleText}>Movement Type</Text>
				<Text style={styles.cardTitleText}>Request Date</Text>
			</View>
			<View style={{ flex: 2, alignItems: "flex-end" }}>
				<Text style={styles.cardDescriptionText}>{TR_TP_HEADER_SAP_DOC}</Text>
				<Text style={styles.cardDescriptionText}>{TR_TP_HEADER_CREATED_BY}</Text>
				<Text style={styles.cardDescriptionText}>{TR_TP_HEADER_MVT_CODE}</Text>
				<Text style={styles.cardDescriptionText}>{moment(TR_TP_HEADER_CREATED_TIMESTAMP).format('YYYY-MM-DD')}</Text>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const CardDisplaySO = (props) => {
	const { data } = props
	const {
		TR_PID_HEADER_ID,
		TR_PID_HEADER_SAP_NO,
		TR_PID_HEADER_SAP_RETURN_YEAR,
		TR_PID_HEADER_PLANT,
		TR_PID_HEADER_CREATED_TIMESTAMP
	} = data
	return (
		<TouchableOpacity style={styles.cardContainer} onPress={() => Actions.detailHistoryPID({ headerID: TR_PID_HEADER_ID })}>
			<View style={{ flex: 3 }}>
				<Text style={styles.cardTitleText}>Document SAP</Text>
				<Text style={styles.cardTitleText}>Document Year</Text>
				<Text style={styles.cardTitleText}>Plant</Text>
				<Text style={styles.cardTitleText}>Created Date</Text>
			</View>
			<View style={{ flex: 2, alignItems: "flex-end" }}>
				<Text style={styles.cardDescriptionText}>{TR_PID_HEADER_SAP_NO}</Text>
				<Text style={styles.cardDescriptionText}>{TR_PID_HEADER_SAP_RETURN_YEAR}</Text>
				<Text style={styles.cardDescriptionText}>{TR_PID_HEADER_PLANT}</Text>
				<Text style={styles.cardDescriptionText}>{moment(TR_PID_HEADER_CREATED_TIMESTAMP).format('YYYY-MM-DD')}</Text>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const ReportScreen = (props) => {
	const [scrollPosition, setScrollPosition] = useState(0)
	const [isLoading, setIsLoading] = useState(false)
	const [isStartDate, setIsStartDate] = useState(false)
	const [incorrectDate, setIncorrectDate] = useState(null)
	const [incorrectMessage, setIncorrectMessage] = useState('')
	const [isDateModalVisible, setIsDateModalVisible] = useState(false)
	const [startDate, setStartDate] = useState(moment(new Date).startOf('month').format('YYYY-MM-DD'))
	const [endDate, setEndDate] = useState(moment(new Date).format('YYYY-MM-DD'))

	const [isScannedData, setIsScannedData] = useState(false)
	const [isModalConfirmVisible, setIsModalConfirmVisible] = useState(false)
	const [barcodeIsScanned, setBarcodeIsScanned] = useState(false)
	const [tempReceiverBarcode, setTempReceiverBarcode] = useState('')


	const [expiredDate, setExpiredDate] = useState('')
	const [catatan, setCatatan] = useState('')
	const [newQTYLeft, setNewQTYLeft] = useState('')
	const [qtyLeft, setQTYLeft] = useState('')
	const [UOM, setUOM] = useState('')
	const [selectedBatch, setSelectedBatch] = useState('')
	const [selectedSLOC, setSelectedSLOC] = useState('')

	const [materialCode, setMaterialCode] = useState('')
	const [materialName, setMaterialName] = useState('')
	const [totalQTY, setTotalQTY] = useState('')
	const [GRHeaderID, setGRHeaderID] = useState('')
	const [detailID, setDetailID] = useState('')
	const [catatanReport, setCatatanReport] = useState('')
	const [fotoBase64AbsensiList, setFotoBase64AbsensiList] = useState('')
	const [tempReceiverPhotoReport, setTempReceiverPhotoReport] = useState('')

	const [dataGR, setDataGR] = useState([])
	const [dataGI, setDataGI] = useState([])
	const [dataTransferPosting, setDataTransferPosting] = useState([])
	const [dataSO, setDataSO] = useState([])

	const [isModalTakePhotoVisible, setIsModalTakePhotoVisible] = useState(false)
	const [isGRExpanded, setIsGRExpanded] = useState(false)
	const [isGIExpanded, setIsGIExpanded] = useState(false)
	const [isTPExpanded, setIsTPExpanded] = useState(false)
	const [isSOExpanded, setIsSOExpanded] = useState(false)
	const [isModalScanBarcodeVisible, setIsModalScanBarcodeVisible] = useState(false)

	const toastRef = useRef(null);

	useEffect(() => {
		initialLoad()
	}, [])

	const initialLoad = (data, data2, data3) => {
		const { current } = toastRef
		setIsLoading(true)
		if (data3 == "GR" || isGRExpanded) {
			wait(2000).then(() => {
				let params_data = `start_date=${data != null ? data : startDate}&end_date=${data2 != null ? data2 : endDate}`
				modelTransactions.getGRHistory(params_data, res => {
					const { status, result } = res
					console.log(result.data)
					console.log(res)
					switch (status) {
						case 200:
							setDataGR(result.data)
							break;
						case 500:
							current.showToast('error', "Connection not available")
							break;
						default:
							current.showToast('error', "Connection not available")
							break;
					}
				})
				setIsLoading(false)
			})
		}
		else if (data3 == "GI" || isGIExpanded) {
			wait(2000).then(() => {
				let params_data = `start_date=${data != null ? data : startDate}&end_date=${data2 != null ? data2 : endDate}`
				modelTransactions.getGIHistory(params_data, res => {
					const { status, result } = res
					console.log(result.data)
					console.log(res)
					switch (status) {
						case 200:
							setDataGI(result.data)
							break;
						case 500:
							current.showToast('error', "Connection not available")
							break;
						default:
							current.showToast('error', "Connection not available")
							break;
					}
				})
				setIsLoading(false)
			})
		}
		else if (data3 == "TP" || isTPExpanded) {
			wait(2000).then(() => {
				let params_data = `start_date=${data != null ? data : startDate}&end_date=${data2 != null ? data2 : endDate}`
				modelTransactions.getTPHistory(params_data, res => {
					const { status, result } = res
					console.log(result.data)
					console.log(res)
					switch (status) {
						case 200:
							setDataTransferPosting(result.data)
							break;
						case 500:
							current.showToast('error', "Connection not available")
							break;
						default:
							current.showToast('error', "Connection not available")
							break;
					}
				})
				setIsLoading(false)
			})
		}
		else if (data3 == "PID" || isSOExpanded) {
			wait(2000).then(() => {
				let params_data = `start_date=${data != null ? data : startDate}&end_date=${data2 != null ? data2 : endDate}`
				modelTransactions.getPIDHistory(params_data, res => {
					const { status, result } = res
					console.log(result.data)
					console.log(res)
					switch (status) {
						case 200:
							setDataSO(result.data)
							break;
						case 500:
							current.showToast('error', "Connection not available")
							break;
						default:
							current.showToast('error', "Connection not available")
							break;
					}
				})
				setIsLoading(false)
			})
		}
	}

	const funcGetDate = (res) => {
		if (!isStartDate) {
			if (res.dateString > endDate) {
				setStartDate(moment().startOf('month').format('YYYY-MM-DD'))
				setEndDate(moment(new Date).format('YYYY-MM-DD'))
				setIsStartDate(false)
				setIncorrectDate(new Date)
				setIncorrectMessage("Start date must be smaller than end date")
			} else {
				setStartDate(res.dateString)
				setIsStartDate(true)
			}
		}
		else {
			if (startDate > res.dateString) {
				setStartDate(moment().startOf('month').format('YYYY-MM-DD'))
				setEndDate(moment(new Date).format('YYYY-MM-DD'))
				setIsStartDate(false)
				setIncorrectDate(new Date)
				setIncorrectMessage("Start date must be smaller than end date")
			}
			else {
				setEndDate(res.dateString)
				setIsStartDate(false)
				setIsDateModalVisible(false)
				initialLoad(startDate, res.dateString)
			}
		}
	}

	const onScanBarcode = (barcodeData) => {
		const { current } = toastRef
		if (barcodeData == "") {
			setTempReceiverBarcode(barcodeData)
			setBarcodeIsScanned(false)
			setIsScannedData(false)
			setIsLoading(false)
		}
		else {
			let params_data = `TR_GR_DETAIL_QR_CODE_NUMBER=${barcodeData}`
			modelTransactions.getPIDScanQR(params_data, res => {
				const { status, result } = res
				console.log(res)
				console.log(result.data.gr_detail_data)
				switch (status) {
					case 200:
						if (result.data.gr_detail_data.length != 0) {
							setIsScannedData(true)
							setExpiredDate(result.data.gr_detail_data.TR_GR_DETAIL_EXP_DATE)
							setTotalQTY(result.data.gr_detail_data.TR_GR_DETAIL_BASE_QTY)
							setQTYLeft(result.data.gr_detail_data.TR_GR_DETAIL_LEFT_QTY)
							setUOM(result.data.gr_detail_data.TR_GR_DETAIL_UOM)
							setSelectedBatch(result.data.gr_detail_data.TR_GR_DETAIL_SAP_BATCH)
							setSelectedSLOC(result.data.gr_detail_data.TR_GR_DETAIL_SLOC)
							setMaterialCode(result.data.gr_detail_data.TR_GR_DETAIL_MATERIAL_CODE)
							setMaterialName(result.data.gr_detail_data.TR_GR_DETAIL_MATERIAL_NAME)
							setDetailID(result.data.gr_detail_data.TR_GR_DETAIL_ID)
							setGRHeaderID(result.data.gr_detail_data.TR_GR_DETAIL_HEADER_ID)
							setTempReceiverBarcode(barcodeData)
							setBarcodeIsScanned(true)
							setIsModalScanBarcodeVisible(false)
							setIsLoading(false)
						}
						else {
							current.showToast('warning', "Mohon pindai QR Code yang valid")
							setTempReceiverBarcode('')
							setBarcodeIsScanned(false)
							setIsModalScanBarcodeVisible(false)
							setIsLoading(false)
						}
						break;
					case 500:
						current.showToast('error', "Connection not available")
						setIsModalScanBarcodeVisible(false)
						setIsLoading(false)
						break;
					default:
						current.showToast('error', "Connection not available")
						setIsModalScanBarcodeVisible(false)
						setIsLoading(false)
						break;
				}
			})
		}
	}


	const handleScanbarcode = () => {
		setIsModalScanBarcodeVisible(true)
	}

	const GRExpand = (val) => {
		if (val) {
			setIsGRExpanded(false)
		}
		else {
			setIsGRExpanded(true)
			setIsGIExpanded(false)
			setIsTPExpanded(false)
			setIsSOExpanded(false)
			initialLoad(startDate, endDate, 'GR')
		}
	}

	const GIExpand = (val) => {
		if (val) {
			setIsGIExpanded(false)
		}
		else {
			setIsGIExpanded(true)
			setIsGRExpanded(false)
			setIsTPExpanded(false)
			setIsSOExpanded(false)
			initialLoad(startDate, endDate, 'GI')
		}
	}

	const TPExpand = (val) => {
		if (val) {
			setIsTPExpanded(false)
		}
		else {
			setIsTPExpanded(true)
			setIsGIExpanded(false)
			setIsGRExpanded(false)
			setIsSOExpanded(false)
			initialLoad(startDate, endDate, "TP")
		}
	}

	const SOExpand = (val) => {
		if (val) {
			setIsSOExpanded(false)
		}
		else {
			setIsSOExpanded(true)
			setIsTPExpanded(false)
			setIsGIExpanded(false)
			setIsGRExpanded(false)
			initialLoad(startDate, endDate, "PID")
		}
	}

	const handleTakePhoto = () => {
		setIsModalTakePhotoVisible(true)
	}

	const onPictureTaken = (cameraData) => {
		const {
			uri,
			base64
		} = cameraData
		setFotoBase64AbsensiList(base64)
		setTempReceiverPhotoReport(uri)
		setIsModalTakePhotoVisible(false)
	}

	const handleSubmitMaterial = () => {
		const { current } = toastRef
		if (newQTYLeft == '') {
			current.showToast('warning', "Mohon masukan sisa kuantitas baru!")
		}
		else if (fotoBase64AbsensiList == '') {
			current.showToast('warning', "Mohon ambil foto!")
		}
		else if (catatan == '') {
			current.showToast('warning', "Mohon masukan catatan!")
		}
		else {
			setIsModalConfirmVisible(true)
		}
	}

	const clearMaterial = () => {
		setIsScannedData(false)
		setTempReceiverBarcode("")
	}


	const onSubmitMaterial = () => {
		const { current } = toastRef
		setIsLoading(true)
		const post_data = new FormData();
		post_data.append('TR_MANUAL_ADJUSTMENT_GR_HEADER_ID', GRHeaderID)
		post_data.append('TR_MANUAL_ADJUSTMENT_GR_DETAIL_ID', detailID)
		post_data.append('TR_MANUAL_ADJUSTMENT_QTY_BEFORE', qtyLeft)
		post_data.append('TR_MANUAL_ADJUSTMENT_QTY_AFTER', newQTYLeft)
		post_data.append('TR_MANUAL_ADJUSTMENT_UOM', UOM)
		post_data.append('TR_MANUAL_ADJUSTMENT_NOTES', catatan)
		post_data.append('photo', fotoBase64AbsensiList)
		console.log(post_data)
		modelTransactions.ReportSubmit(post_data, res => {
			const { status, result } = res
			console.log(res)
			switch (status) {
				case 200:
					setIsModalConfirmVisible(false)
					setIsLoading(true)
					wait(2000).then(() => {
						current.showToast('success', "Report has been submitted successfully!")
						wait(2000).then(() => {
							setIsLoading(false)
							setTempReceiverBarcode('')
							setBarcodeIsScanned(false)
							setIsScannedData(false)
						})
					})
					break;
				case 400:
					setIsModalConfirmVisible(false)
					setIsLoading(false)
					current.showToast('warning', "Data already Submitted, Transaction Cancelled")
					break;
				default:
					setIsModalConfirmVisible(false)
					setIsLoading(false)
					current.showToast('warning', `*${result.message}*`)
					break;
			}
		})
	}

	return (
		<View style={{ flex: 1 }}>
			<ScrollView
				onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}>
				<View style={styles.formHeaderContentContainer}>
					<CustomInputComponent
						onEndEditing={() => onScanBarcode(tempReceiverBarcode)}
						label='Batch Number'
						value={tempReceiverBarcode}
						placeholder='Input batch number'
						onChangeText={setTempReceiverBarcode}

					/>
					{tempReceiverBarcode == "" ?
						<CustomButton
							customColor={Colors.BLUE_LIGHT}
							onPress={() => handleScanbarcode()}
							label='Scan QR Code'
						/>
						:
						<CustomButton
							disabled
							customColor={Colors.BLUE_LIGHT}
							onPress={() => handleScanbarcode()}
							label='Scan QR Code'
						/>
					}
				</View>

				{isScannedData ?
					<>
						<View style={GlobalStyle.formHeaderContentContainer}>
							<CustomInputComponent
								disabled
								label='Batch'
								value={selectedBatch}
							/>
							<CustomInputComponent
								disabled
								label='Material Code'
								value={materialCode}
							/>
							<CustomInputComponent
								disabled
								label='Material Name'
								value={materialName}
							/>
							<CustomInputComponent
								disabled
								label='Storage Location'
								value={selectedSLOC}
							/>
							<CustomInputComponent
								disabledTextInput
								label='Total Quantity'
								suffix={UOM}
								value={totalQTY}
							/>
							<CustomInputComponent
								disabledTextInput
								label='Quantity Left'
								suffix={UOM}
								value={qtyLeft}
							/>
							<CustomInputComponent
								label='New Quantity Left'
								suffix={UOM}
								value={newQTYLeft}
								placeholder='Input material quantity'
								onChangeText={inputValidation(setNewQTYLeft, 'number')}
								keyboardType='numeric'
							/>
							<CustomInputComponent
								disabled
								isDate
								onDatePress={() => changeDate()}
								label='Expired Date'
								value={expiredDate}
							/>
							<View style={GlobalStyle.formContentContainer}>
								<CustomInputComponent
									isVertical
									label='Notes'
									isLabelBold
									value={catatan}
									placeholder='Notes'
									onChangeText={setCatatan}
								/>
							</View>
							<View style={[GlobalStyle.formButtonContainer, { paddingHorizontal: 0 }]}>
								<CustomButton
									customColor={Colors.GREEN_LIGHT}
									onPress={() => handleTakePhoto()}
									label='Take Photo'
								/>
								{tempReceiverPhotoReport ? (
									<View style={styles.tempImageStyle}>
										<Image resizeMethod="resize" resizeMode='cover' source={{ uri: tempReceiverPhotoReport }} style={{ height: '100%', width: '100%' }} />
									</View>
								) : null}
							</View>
							<View style={[GlobalStyle.formButtonContainer, { flexDirection: "row" }]}>
								<CustomButton
									onPress={handleSubmitMaterial}
									isLoading={isLoading}
									label='Submit'
								/>
								<View style={{ width: 10 }}></View>
								<CustomButton
									customColor={Colors.RED}
									onPress={clearMaterial}
									isLoading={isLoading}
									label='Back'
								/>
							</View>
						</View>
					</>
					:
					<>
						<TouchableOpacity
							onPress={() => setIsDateModalVisible(true)}
							style={styles.dateStyle}>
							<Text style={{ fontSize: 14, fontFamily: Fonts.ROBOTO_REGULAR, letterSpacing: 0.5, lineHeight: 24, color: Colors.SemiBlackColor }}>Select Start Date</Text>
							<View style={{ alignItems: 'center', flexDirection: 'row' }}>
								<Ionicons name="calendar-outline" size={22} color="black" />
								<Text style={{ fontSize: 14, fontFamily: Fonts.ROBOTO_REGULAR, letterSpacing: 0.5, lineHeight: 24, color: Colors.SemiBlackColor }}> {moment(startDate).format('DD MMMM')} - {moment(endDate).format('DD MMMM')}</Text>
							</View>
						</TouchableOpacity>
						<View style={{
							borderBottomWidth: 4,
							borderBottomColor: Colors.GRAY_LIGHT
						}}>
							<TouchableOpacity
								style={{
									width: '100%',
									height: 60,
									flexDirection: 'row',
									justifyContent: 'space-between',
									alignItems: 'center',
									paddingHorizontal: 20
								}}
								onPress={() => GRExpand(isGRExpanded)}
							>
								<Text style={{ fontFamily: Fonts.ROBOTO_MEDIUM, fontSize: 18, color: Colors.BLACK }}>Good Receipt History</Text>
								<Ionicons
									name={isGRExpanded ? 'chevron-up-outline' : 'chevron-down-outline'}
									size={30}
									color={Colors.DarkColor}
								/>
							</TouchableOpacity>
							{isGRExpanded &&
								<FlatList
									refreshControl={(
										<RefreshControl
											refreshing={isLoading}
											onRefresh={initialLoad}
										/>
									)}
									data={dataGR}
									extraData={dataGR}
									keyExtractor={item => String(item.TR_GR_HEADER_ID)}
									contentContainerStyle={GlobalStyle.viewContainer}
									// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
									onEndReachedThreshold={0.1}
									ListEmptyComponent={(
										<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
											<View style={styles.illustContainerImage}>
												<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
												<View style={styles.titleTextContainer}>
													<Text style={styles.illustTextDesc}>History is empty</Text>
												</View>
											</View>
										</View>
									)}
									renderItem={({ item, index }) => (
										<CardDisplayGR
											data={item} />
									)}
								// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
								// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
								/>
							}
						</View>

						<View style={{
							borderBottomWidth: 4,
							borderBottomColor: Colors.GRAY_LIGHT
						}}>
							<TouchableOpacity
								style={{
									width: '100%',
									height: 60,
									flexDirection: 'row',
									justifyContent: 'space-between',
									alignItems: 'center',
									paddingHorizontal: 20
								}}
								onPress={() => GIExpand(isGIExpanded)}
							>
								<Text style={{ fontFamily: Fonts.ROBOTO_MEDIUM, fontSize: 18, color: Colors.BLACK }}>Good Issue History</Text>
								<Ionicons
									name={isGIExpanded ? 'chevron-up-outline' : 'chevron-down-outline'}
									size={30}
									color={Colors.DarkColor}
								/>
							</TouchableOpacity>
							{isGIExpanded &&
								<View style={[styles.formButtonContainer, { flexDirection: "row" }]}>
									<FlatList
										refreshControl={(
											<RefreshControl
												refreshing={isLoading}
												onRefresh={initialLoad}
											/>
										)}
										data={dataGI}
										extraData={dataGI}
										keyExtractor={item => String(item.TR_GI_SAPHEADER_ID)}
										contentContainerStyle={GlobalStyle.viewContainer}
										// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
										onEndReachedThreshold={0.1}
										ListEmptyComponent={(
											<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
												<View style={styles.illustContainerImage}>
													<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
													<View style={styles.titleTextContainer}>
														<Text style={styles.illustTextDesc}>History is empty</Text>
													</View>
												</View>
											</View>
										)}
										renderItem={({ item, index }) => (
											<CardDisplayGI
												data={item} />
										)}
									// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
									// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
									/>
								</View>
							}
						</View>

						<View style={{
							borderBottomWidth: 4,
							borderBottomColor: Colors.GRAY_LIGHT
						}}>
							<TouchableOpacity
								style={{
									width: '100%',
									height: 60,
									flexDirection: 'row',
									justifyContent: 'space-between',
									alignItems: 'center',
									paddingHorizontal: 20
								}}
								onPress={() => TPExpand(isTPExpanded)}
							>
								<Text style={{ fontFamily: Fonts.ROBOTO_MEDIUM, fontSize: 18, color: Colors.BLACK }}>Transfer Posting History</Text>
								<Ionicons
									name={isTPExpanded ? 'chevron-up-outline' : 'chevron-down-outline'}
									size={30}
									color={Colors.DarkColor}
								/>
							</TouchableOpacity>
							{isTPExpanded &&
								<View style={[styles.formButtonContainer, { flexDirection: "row" }]}>
									<FlatList
										refreshControl={(
											<RefreshControl
												refreshing={isLoading}
												onRefresh={initialLoad}
											/>
										)}
										data={dataTransferPosting}
										extraData={dataTransferPosting}
										keyExtractor={item => String(item.TR_TP_HEADER_ID)}
										contentContainerStyle={GlobalStyle.viewContainer}
										// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
										onEndReachedThreshold={0.1}
										ListEmptyComponent={(
											<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
												<View style={styles.illustContainerImage}>
													<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
													<View style={styles.titleTextContainer}>
														<Text style={styles.illustTextDesc}>History is empty</Text>
													</View>
												</View>
											</View>
										)}
										renderItem={({ item, index }) => (
											<CardDisplayTP
												data={item} />
										)}
									// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
									// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
									/>
								</View>
							}
						</View>

						<View style={{
							borderBottomWidth: 4,
							borderBottomColor: Colors.GRAY_LIGHT
						}}>
							<TouchableOpacity
								style={{
									width: '100%',
									height: 60,
									flexDirection: 'row',
									justifyContent: 'space-between',
									alignItems: 'center',
									paddingHorizontal: 20
								}}
								onPress={() => SOExpand(isSOExpanded)}
							><Text style={{ fontFamily: Fonts.ROBOTO_MEDIUM, fontSize: 18, color: Colors.BLACK }}>Stock Opname History</Text>
								<Ionicons
									name={isTPExpanded ? 'chevron-up-outline' : 'chevron-down-outline'}
									size={30}
									color={Colors.DarkColor}
								/>
							</TouchableOpacity>
							{isSOExpanded &&
								<View style={[styles.formButtonContainer, { flexDirection: "row" }]}>
									<FlatList
										refreshControl={(
											<RefreshControl
												refreshing={isLoading}
												onRefresh={initialLoad}
											/>
										)}
										data={dataSO}
										extraData={dataSO}
										keyExtractor={item => String(item.TR_PID_HEADER_ID)}
										contentContainerStyle={GlobalStyle.viewContainer}
										// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
										onEndReachedThreshold={0.1}
										ListEmptyComponent={(
											<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
												<View style={styles.illustContainerImage}>
													<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
													<View style={styles.titleTextContainer}>
														<Text style={styles.illustTextDesc}>History is empty</Text>
													</View>
												</View>
											</View>
										)}
										renderItem={({ item, index }) => (
											<CardDisplaySO
												data={item} />
										)}
									// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
									// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
									/>
								</View>
							}
						</View>
					</>
				}
			</ScrollView>
			<CustomModalConfirm
				title='Save Material'
				body='Are you sure want to save change? Make sure your data is correct.'
				onConfirm={onSubmitMaterial}
				onCancel={() => setIsModalConfirmVisible(false)}
				isModalVisible={isModalConfirmVisible}
			/>
			<CustomModalScanBarcode
				onScanBarcode={onScanBarcode}
				onCancel={() => setIsModalScanBarcodeVisible(false)}
				isModalVisible={isModalScanBarcodeVisible}
			/>
			<CustomModalCamera
				onPictureTaken={onPictureTaken}
				onCancel={() => setIsModalTakePhotoVisible(false)}
				isModalVisible={isModalTakePhotoVisible}
			/>
			<CustomCalendar
				message={incorrectMessage}
				lastUpdate={incorrectDate}
				isStartDate={isStartDate}
				onBackdropPress={() => setIsDateModalVisible(false)}
				currentDate={startDate}
				endDate={endDate}
				funcGetDate={funcGetDate}
				isDateModalVisible={isDateModalVisible}
			/>
			<CustomToast ref={toastRef} />
		</View >
	)
}

export default ReportScreen

const styles = StyleSheet.create({
	illustContainerImage: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	illustStyle: {
		width: 200,
		height: 200,
		resizeMode: 'cover',
	},
	illustTextDesc: {
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontSize: 14,
		textAlign: 'center',
		marginTop: 20,
		color: Colors.BLACK,
	},
	dateStyle: {
		backgroundColor: Colors.WhiteColor,
		height: 60,
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		borderBottomWidth: 3,
		borderBottomColor: Colors.GRAY_LIGHT,
	},
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	formHeaderContentContainer: {
		paddingTop: 20,
		paddingHorizontal: 20,
		paddingBottom: 20,
		borderBottomWidth: 4,
		borderBottomColor: Colors.GRAY_LIGHT
	},
	formButtonContainer: {
		paddingHorizontal: 20,
		paddingBottom: 20,
	},
	cardTitleText: {
		fontSize: 14,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: 'bold'
	},
	cardDescriptionText: {
		fontSize: 12,
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontWeight: 'bold',
		marginBottom: 4
	},
	progressBarContainer: {
		height: 10,
		flexDirection: 'row',
		marginVertical: 20,
		borderRadius: 8,
		backgroundColor: Colors.GRAY,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.18,
		shadowRadius: 1.00,

		elevation: 1
	},
	indicatorContainer: {
		height: 6,
		width: 20,
		marginTop: 1,
		marginRight: 10,
		borderRadius: 4,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.18,
		shadowRadius: 1.00,

		elevation: 1,
	},
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	cardTitleText: {
		fontSize: 14,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: 'bold'
	},
	modalContainer: {
		backgroundColor: Colors.WHITE,
		borderRadius: 10,
		overflow: 'hidden'
	},
	modalHeaderContainer: {
		backgroundColor: Colors.GREEN_DARK,
		paddingVertical: 20,
		paddingHorizontal: 30,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	modalContentContainer: {
		paddingVertical: 20,
		paddingHorizontal: 30,
	},
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	buttonTextContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingLeft: 20
	},
	buttonText: {
		...GlobalStyle.textFontSize14,
		fontFamily: Fonts.ROBOTO_MEDIUM,
		fontWeight: "bold",
		letterSpacing: 0.2,
		color: Colors.WHITE
	},
	iconStyle: {
		position: 'absolute',
		left: 8,
		top: 6,
		fontSize: 24,
		color: Colors.WHITE
	},
	itemButtonContainer: {
		height: 28,
		paddingHorizontal: 15,
		borderRadius: 6,
		justifyContent: 'center'
	},
	itemButtonText: {
		fontSize: 12,
		fontFamily: Fonts.ROBOTO_BOLD,
		fontWeight: 'bold',
		letterSpacing: 0.5,
		color: Colors.WHITE
	},
	indicatorContainer: {
		height: 6,
		width: 20,
		marginTop: 1,
		marginRight: 10,
		borderRadius: 4,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.18,
		shadowRadius: 1.00,

		elevation: 1,
	},
	cardContainer: {
		flexDirection: "row",
		marginBottom: 20,
		marginHorizontal: 5,
		paddingVertical: 10,
		paddingHorizontal: 15,
		borderRadius: 5,
		backgroundColor: Colors.WHITE,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.50,
		shadowRadius: 1.45,
		elevation: 2,
	},
})