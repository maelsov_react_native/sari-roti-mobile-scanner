import React, { useState } from 'react'
import { ScrollView, View, StyleSheet, Image } from 'react-native'
import { Actions } from 'react-native-router-flux';

import { Colors } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { inputValidation } from '../../../../globals/GlobalFunction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomButton from '../../../../components/CustomButton';

const DetailHistoryGRMaterialScreen = (props) => {
	const {selectedBatch,
		 materialCode,
		 materialName,
		 selectedSLOC,
		 UOM,
		 totalQTY,
		 receivedQuantity,
		 qtyLeft,
		 expiredDate,
		 catatan,
		 tempReceiverIDPhoto } = props
	const [scrollPosition,
		 setScrollPosition] = useState(0)

	return (
		<View style={{ flex: 1 }}>
			<ScrollView
				onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}>
				<View style={GlobalStyle.formHeaderContentContainer}>
					<CustomInputComponent
						disabled
						label='Batch Number'
						value={selectedBatch}
					/>
					<CustomInputComponent
						disabled
						label='Material Code'
						value={materialCode}
					/>
					<CustomInputComponent
						disabled
						label='Material Name'
						value={materialName}
					/>
					<CustomInputComponent
						disabled
						label='Storage Location'
						value={selectedSLOC}
					/>
					<CustomInputComponent
						disabledTextInput
						label='Total Quantity'
						suffix={UOM}
						value={totalQTY}
					/>
					<CustomInputComponent
						disabledTextInput
						label='Quantity Left'
						suffix={UOM}
						value={qtyLeft}
					/>
					<CustomInputComponent
						disabledTextInput
						label='Received Quantity'
						suffix={UOM}
						value={receivedQuantity}
					/>
					<CustomInputComponent
						disabled
						isDate
						label='Expired Date'
						value={expiredDate}
					/>
					<View style={GlobalStyle.formContentContainer}>
						<CustomInputComponent
							disabled
							isVertical
							label='Notes'
							isLabelBold
							value={catatan}
							placeholder='Notes'
						/>
					</View>
				</View>
				{tempReceiverIDPhoto != null && tempReceiverIDPhoto != "" &&
					<View style={GlobalStyle.formContentContainer}>
						<View style={[GlobalStyle.formButtonContainer, { paddingHorizontal: 0 }]}>
							<View style={styles.tempImageStyle}>
								<Image resizeMethod="resize" resizeMode='cover' source={{ uri: tempReceiverIDPhoto }} style={{ height: '100%', width: '100%' }} />
							</View>
						</View>
					</View>
				}
				<View style={GlobalStyle.formButtonContainer}>
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						label='Back'
					/>
				</View>
			</ScrollView>
		</View>
	)
}

export default DetailHistoryGRMaterialScreen

const styles = StyleSheet.create({
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
})