import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, View, StyleSheet, Image, Text, RefreshControl, TouchableOpacity, FlatList } from 'react-native'
import { Actions } from 'react-native-router-flux';

import { Colors, Fonts, Icons, Illustrations, Metrics } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { wait } from '../../../../globals/GlobalFunction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomButton from '../../../../components/CustomButton';
import { modelTransactions } from '../../../../models/Transaction';

const CardDisplay = (props) => {
	const { data } = props
	const {
		TR_GI_SAPDETAIL_MATERIAL_NAME,
		TR_GI_SAPDETAIL_MATERIAL_CODE,
		TR_GI_SAPDETAIL_CREATED_TIMESTAMP,
		TR_GI_SAPDETAIL_SLOC,

		TR_GI_SAPDETAIL_SAP_BATCH,
		TR_GI_SAPDETAIL_NOTES,
		TR_GI_SAPDETAIL_MOBILE_QTY,
		TR_GI_SAPDETAIL_MOBILE_UOM,
		TR_GI_SAPDETAIL_PHOTO,
		TR_GI_SAPDETAIL_QR_CODE_NUMBER,
		TR_GI_SAPDETAIL_GI_QTY,
	} = data


	const goToDetailMaterial = () => {
		Actions.detailHistoryMaterialGI({ selectedBatch: TR_GI_SAPDETAIL_SAP_BATCH, materialCode: TR_GI_SAPDETAIL_MATERIAL_CODE, materialName: TR_GI_SAPDETAIL_MATERIAL_NAME, selectedSLOC: TR_GI_SAPDETAIL_SLOC, UOM: TR_GI_SAPDETAIL_MOBILE_UOM, totalQTY: TR_GI_SAPDETAIL_GI_QTY, receivedQuantity: TR_GI_SAPDETAIL_MOBILE_QTY, catatan: TR_GI_SAPDETAIL_NOTES, tempReceiverIDPhoto: TR_GI_SAPDETAIL_PHOTO, qrCode: TR_GI_SAPDETAIL_QR_CODE_NUMBER })
	}
	return (
		<TouchableOpacity
			style={{ flexDirection: "row", paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: Colors.GRAY_DARK, marginBottom: 20 }}
			onPress={() => goToDetailMaterial()}>
			<View style={{ flex: 4 }}>
				<CustomInputComponent
					disabled
					label='Material Name'
					value={TR_GI_SAPDETAIL_MATERIAL_NAME}
				/>
				<CustomInputComponent
					disabled
					label='Material Code'
					value={TR_GI_SAPDETAIL_MATERIAL_CODE}
				/>
				<CustomInputComponent
					disabled
					label='Storage Location'
					value={TR_GI_SAPDETAIL_SLOC}
				/>
				<CustomInputComponent
					disabled
					isDate
					label='Created Date'
					value={TR_GI_SAPDETAIL_CREATED_TIMESTAMP}
				/>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const DetailHistoryGIScreen = (props) => {
	const [scrollPosition, setScrollPosition] = useState(0)

	const [PONumber, setPONumber] = useState('')
	const [MVTCodeHeader, setMVTCodeHeader] = useState('')
	const [createdDateHeader, setCreatedDateHeader] = useState('')
	const [DOCDateHeader, setDOCDateHeader] = useState('')
	const [createdByHeader, setCreatedByHeader] = useState('')
	const [photoHeader, setPhotoHeader] = useState('')
	const [postingDateHeader, setPostingDateHeader] = useState('')
	const [supplyingPlant, setSupplyingPlant] = useState('')

	const [materialList, setMaterialList] = useState([])
	const [filteredMaterialList, setFilteredMaterialList] = useState([])
	const [filteredMaterialListIndex, setFilteredMaterialListIndex] = useState("5")

	const [isLoading, setIsLoading] = useState(false)

	const toastRef = useRef(null);

	useEffect(() => {
		initialLoad()
	}, [])

	const initialLoad = () => {
		wait(2000).then(() => {
			let params_data = `TR_GI_SAPHEADER_ID=${props.sapHeaderID}`
			modelTransactions.getGIDetailHistory(params_data, res => {
				const { status, result } = res
				console.log(result.data)
				switch (status) {
					case 200:
						setPONumber(result.data.header.TR_GI_SAPHEADER_PO_NUMBER)
						setMVTCodeHeader(result.data.header.TR_GI_SAPHEADER_MVT_CODE)
						setCreatedDateHeader(result.data.header.TR_GI_SAPHEADER_CREATED_TIMESTAMP)
						setCreatedByHeader(result.data.header.TR_GI_SAPHEADER_CREATED_BY)
						setDOCDateHeader(result.data.header.TR_GI_SAPHEADER_DOC_DATE)
						setPostingDateHeader(result.data.header.TR_GI_SAPHEADER_PSTG_DATE)
						setPhotoHeader(result.data.header.TR_GI_SAPHEADER_PHOTO)
						setSupplyingPlant(result.data.header.TR_GI_SAPHEADER_CREATED_PLANT_CODE)

						setMaterialList(result.data.detail)
						if (result.data.detail.length > 5) {
							setFilteredMaterialList(result.data.detail.slice(0, filteredMaterialListIndex))
						}
						else {
							setFilteredMaterialList(result.data.detail)
						}
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const loadOtherIndex = () => {
		setIsLoading(true)
		const index = parseInt(filteredMaterialListIndex) + 5
		if (index < materialList.length) {
			setFilteredMaterialListIndex(index)
			setFilteredMaterialList(materialList.slice(0, index))
			setIsLoading(false)
		}
		else if (index >= materialList.length) {
			setFilteredMaterialListIndex(materialList.length)
			setFilteredMaterialList(materialList.slice(0, materialList.length))
			setIsLoading(false)
		}
	}

	const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
		const paddingToBottom = 20;
		return layoutMeasurement.height + contentOffset.y >=
			contentSize.height - paddingToBottom;
	};

	return (
		<View style={{ flex: 1 }}>
			<ScrollView>
				<View style={GlobalStyle.formHeaderContentContainer}>
					<CustomInputComponent
						disabled
						// isCustomOption
						label='PO Number'
						value={PONumber}
					/>
					<CustomInputComponent
						disabled
						label='Created By'
						value={createdByHeader}
					/>
					<CustomInputComponent
						disabled
						label='Movement Code'
						value={MVTCodeHeader}
					/>
					<CustomInputComponent
						disabled
						label='Supplying Plant'
						value={supplyingPlant}
					/>
					<CustomInputComponent
						disabled
						isDate
						label='Created Date'
						value={createdDateHeader}
					/>
					<CustomInputComponent
						disabled
						isDate
						label='Document Date'
						value={DOCDateHeader}
					/>
					<CustomInputComponent
						disabled
						isDate
						label='Posting Date'
						value={postingDateHeader}
					/>
				</View>
				{photoHeader != null && photoHeader != "" &&
					<View style={GlobalStyle.formContentContainer}>
						<View style={[GlobalStyle.formButtonContainer, { paddingHorizontal: 0 }]}>
							<View style={styles.tempImageStyle}>
								<Image resizeMethod="resize" resizeMode='cover' source={{ uri: photoHeader }} style={{ height: '100%', width: '100%' }} />
							</View>
						</View>
					</View>
				}
				<View style={GlobalStyle.formContentContainer}>
					<Text style={GlobalStyle.formHeaderTitleText}>List of Material ({materialList.length})</Text>
					<View style={{ marginTop: 10 }}>
						<FlatList
							nestedScrollEnabled={true}
							style={{ maxHeight: Metrics.SCREEN_WIDTH*2 }}
							onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}
							onScrollEndDrag={({ nativeEvent }) => {
								if (isCloseToBottom(nativeEvent) && materialList.length > filteredMaterialListIndex && isLoading === false) {
									loadOtherIndex()
								}
							}}
							scrollEventThrottle={100}
							refreshControl={(
								<RefreshControl
									refreshing={isLoading}
									onRefresh={initialLoad}
								/>
							)}
							data={filteredMaterialList}
							extraData={filteredMaterialList}
							keyExtractor={item => String(item.TR_GI_SAPDETAIL_ID)}
							contentContainerStyle={GlobalStyle.viewContainer}
							// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
							onEndReachedThreshold={0.1}
							ListEmptyComponent={(
								<View style={{ flex: 1, alignItems: 'center' }}>
									<View style={styles.illustContainerImage}>
										<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
										<View style={styles.titleTextContainer}>
											<Text style={styles.illustTextDesc}>Material is empty</Text>
										</View>
									</View>
								</View>
							)}
							renderItem={({ item, index }) => (
								<CardDisplay
									data={item} />
							)}
						// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
						// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
						/>
					</View>
				</View>
				<View style={GlobalStyle.formButtonContainer}>
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						label='Back'
					/>
				</View>
			</ScrollView>
		</View>
	)
}

export default DetailHistoryGIScreen

const styles = StyleSheet.create({
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	illustContainerImage: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	illustStyle: {
		width: 200,
		height: 200,
		resizeMode: 'cover',
	},
	illustTextDesc: {
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontSize: 14,
		textAlign: 'center',
		marginTop: 20,
		color: Colors.BLACK,
	},
})