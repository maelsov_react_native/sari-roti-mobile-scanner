import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, View, Text, StyleSheet, TouchableOpacity, Image, FlatList, RefreshControl } from 'react-native'
import { Actions } from 'react-native-router-flux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

import { Colors, Fonts, Icons, Illustrations, Metrics } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { getTPDataSubmit, wait } from '../../../../globals/GlobalFunction';
import { modelTransactions } from '../../../../models/Transaction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomModalCamera from '../../../../components/CustomModalCamera';
import CustomButton from '../../../../components/CustomButton';
import CustomToast from '../../../../components/CustomToast';
import CustomModalConfirm from '../../../../components/CustomModalConfirm';

const CardDisplay = (props) => {
	const { data, materialData, mvt } = props
	const {
		TR_TP_DETAIL_ID,
		TR_TP_DETAIL_MATERIAL_NAME,
		TR_TP_DETAIL_BASE_QTY,
		TR_TP_DETAIL_SLOC,
		TR_TP_DETAIL_MATERIAL_CODE,
		TR_TP_DETAIL_BASE_UOM,
		TR_TP_DETAIL_SAP_BATCH,
	} = data

	const isChecked = materialData.findIndex(e => e.headerID == TR_TP_DETAIL_ID) > -1
	const found = materialData.find(e => e.headerID == TR_TP_DETAIL_ID)
	const goToDetailMaterial = () => {
		if (isChecked) {
			Actions.detailMaterialTransferPosting({ headerID: TR_TP_DETAIL_ID, sapBatch: TR_TP_DETAIL_SAP_BATCH, materialCode: TR_TP_DETAIL_MATERIAL_CODE, materialName: TR_TP_DETAIL_MATERIAL_NAME, storageLocation: TR_TP_DETAIL_SLOC, totalQTY: TR_TP_DETAIL_BASE_QTY, foto: found.photo, foto2: found.photoTemp, note: found.note, qty: found.qty, uom: found.uom, submited: true, batchNumber: found.batchNumber, mvt: mvt })
		}
		else {
			Actions.detailMaterialTransferPosting({ headerID: TR_TP_DETAIL_ID, sapBatch: TR_TP_DETAIL_SAP_BATCH, materialCode: TR_TP_DETAIL_MATERIAL_CODE, materialName: TR_TP_DETAIL_MATERIAL_NAME, storageLocation: TR_TP_DETAIL_SLOC, totalQTY: TR_TP_DETAIL_BASE_QTY, uom: TR_TP_DETAIL_BASE_UOM, mvt: mvt })
		}
	}
	return (
		<TouchableOpacity
			style={{ flexDirection: "row", paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: Colors.GRAY_DARK, marginBottom: 20 }}
			onPress={() => goToDetailMaterial()}>
			<View style={{ flex: 4 }}>
				<CustomInputComponent
					disabled
					label='Material'
					value={TR_TP_DETAIL_MATERIAL_NAME}
				/>
				<CustomInputComponent
					disabledTextInput
					suffix={TR_TP_DETAIL_BASE_UOM}
					label='Quantity'
					value={TR_TP_DETAIL_BASE_QTY}
				/>
				<CustomInputComponent
					disabled
					label='Sloc'
					value={TR_TP_DETAIL_SLOC}
				/>
				{isChecked &&
					<CustomInputComponent
						disabledTextInput
						suffix={TR_TP_DETAIL_BASE_UOM}
						label='Received Quantity'
						value={found.qty}
					/>
				}
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				{isChecked && (
					<FontAwesome
						name='check'
						color={Colors.GREEN}
						size={16}
						style={{ position: "absolute", top: 5, right: 5, height: 20, width: 20 }}
					/>
				)
				}
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const DetailTransferPosting = (props) => {
	const { lastUpdate } = props
	const [scrollPosition, setScrollPosition] = useState(0)

	const [headerID, setHeaderID] = useState(props.headerID)
	const [SAPDoc, setSAPDoc] = useState('')
	const [MVT, setMVT] = useState('')
	const [plant, setPlant] = useState('')
	const [createdBy, setCreatedBy] = useState('')
	const [createdTime, setCreatedTime] = useState('')
	const [BOL, setBOL] = useState('')
	const [catatan, setCatatan] = useState('')
	const [postingDate, setPostingDate] = useState('')
	const [mode, setMode] = useState('')

	const [fotoBase64AbsensiList, setFotoBase64AbsensiList] = useState('')
	const [tempReceiverIDPhoto, setTempReceiverIDPhoto] = useState('')

	const [materialList, setMaterialList] = useState([])
	const [savedMaterial, setSavedMaterial] = useState([])
	const [filteredMaterialList, setFilteredMaterialList] = useState([])
	const [filteredMaterialListIndex, setFilteredMaterialListIndex] = useState("5")

	const [isLoading, setIsLoading] = useState(false)
	const [isModalConfirmVisible, setIsModalConfirmVisible] = useState(false)
	const [isModalTakePhotoVisible, setIsModalTakePhotoVisible] = useState(false)
	const [isShowDatePicker, setIsShowDatePicker] = useState(false)

	const toastRef = useRef(null);


	useEffect(() => {
		if (lastUpdate) {
			getTPDataSubmit().then(result => {
				if (result != null) {
					setSavedMaterial(result)
					console.log(result)
				}
			})
			initialLoad()
		}
	}, [lastUpdate])

	const initialLoad = () => {
		setIsLoading(true)
		wait(2000).then(() => {
			let params_data = `TR_TP_HEADER_ID=${headerID}`
			modelTransactions.getTPDetail(params_data, res => {
				const { status, result } = res
				console.log(result.data.tp_detail_data)
				console.log("DATA", result.data.tp_detail_data)
				switch (status) {
					case 200:
						setMaterialList(result.data.tp_detail_data)
						if (result.data.tp_detail_data.length > 5) {
							setFilteredMaterialList(result.data.tp_detail_data.slice(0, filteredMaterialListIndex))
						}
						else {
							setFilteredMaterialList(result.data.tp_detail_data)
						}
						setMVT(result.data.tp_header_data.TR_TP_HEADER_MVT_CODE)
						setPlant(result.data.tp_header_data.TR_TP_HEADER_PLANT_CODE)
						setPlant(result.data.tp_header_data.TR_TP_HEADER_PLANT_CODE)
						setSAPDoc(result.data.tp_header_data.TR_TP_HEADER_SAP_DOC)
						setCreatedBy(result.data.tp_header_data.TR_TP_HEADER_CREATED_BY)
						setCreatedTime(moment(result.data.tp_header_data.TR_TP_HEADER_CREATED_TIMESTAMP).format('YYYY-MM-DD'))
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const handleTakePhoto = () => {
		setIsModalTakePhotoVisible(true)
	}

	const onPictureTaken = (cameraData) => {
		const {
			uri,
			base64
		} = cameraData

		setFotoBase64AbsensiList(base64)
		setTempReceiverIDPhoto(uri)
		setIsModalTakePhotoVisible(false)
	}

	const handleSubmitTP = () => {
		const { current } = toastRef
		if (postingDate == '') {
			current.showToast('warning', "Mohon input tanggal posting!")
		}
		else if (tempReceiverIDPhoto === '') {
			current.showToast('warning', "Mohon ambil foto!")
		}
		else if (catatan === '') {
			current.showToast('warning', "Mohon input catatan!")
		}
		else {
			const jumlahData = materialList.reduce((acc, cur) => {
				const found = savedMaterial.findIndex(e => e.headerID == cur.TR_TP_DETAIL_ID) > -1
				if (found) return acc + 1
				else acc
			}, 0)

			const goodData = jumlahData == materialList.length
			console.log(jumlahData)
			console.log(materialList.length)
			console.log(goodData)
			if (goodData) {
				setIsModalConfirmVisible(true)
			}
			else {
				current.showToast('warning', "Mohon submit material lainnya!")
			}
		}
	}

	const onSubmitTransferPosting = () => {
		const { current } = toastRef
		setIsLoading(true)
		const post_data = new FormData();
		post_data.append('TR_TP_HEADER_ID', headerID)
		post_data.append('TR_TP_HEADER_PSTG_DATE', moment(postingDate).format('YYYY-MM-DD'))
		post_data.append('TR_TP_HEADER_TXT', catatan)
		post_data.append('TR_TP_HEADER_BOL', BOL)
		post_data.append('TP_PHOTO', fotoBase64AbsensiList)
		for (let index = 0; index < savedMaterial.length; index++) {
			const element = savedMaterial[index];
			post_data.append('tp_materials[' + index + '][TR_TP_DETAIL_ID]', element.headerID)
			post_data.append('tp_materials[' + index + '][TR_TP_DETAIL_MOBILE_QTY]', element.qty)
			post_data.append('tp_materials[' + index + '][TR_TP_DETAIL_MOBILE_UOM]', element.uom)
			post_data.append('tp_materials[' + index + '][TR_TP_DETAIL_NOTES]', element.note)
			post_data.append('tp_materials[' + index + '][materialPhoto]', element.photo)
		}
		modelTransactions.TPSubmit(post_data, res => {
			const { status, result } = res
			console.log(res)
			switch (status) {
				case 200:
					setIsModalConfirmVisible(false)
					wait(2000).then(() => {
						current.showToast('success', "Transfer Posting has been submitted successfully!")
						wait(2000).then(() => {
							setIsLoading(false)
							Actions.pop()
						})
					})
					break;
				case 400:
					setIsLoading(false)
					setIsModalConfirmVisible(false)
					current.showToast('warning', "Data already Submitted, Transaction Cancelled")
					break;
				default:
					setIsLoading(false)
					setIsModalConfirmVisible(false)
					current.showToast('warning', `*${result.message}*`)
					break;
			}
		})
	}

	const changeDate = () => {
		setIsShowDatePicker(true)
	}

	const onChangeDate = (event, selectedDate) => {
		const currentDate = selectedDate || date;
		console.log(currentDate)
		setIsShowDatePicker(false);
		setPostingDate(currentDate);
	};

	const loadOtherIndex = () => {
		setIsLoading(true)
		const index = parseInt(filteredMaterialListIndex) + 5
		if (index < materialList.length) {
			setFilteredMaterialListIndex(index)
			setFilteredMaterialList(materialList.slice(0, index))
			setIsLoading(false)
		}
		else if (index >= materialList.length) {
			setFilteredMaterialListIndex(materialList.length)
			setFilteredMaterialList(materialList.slice(0, materialList.length))
			setIsLoading(false)
		}
	}

	const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
		const paddingToBottom = 20;
		return layoutMeasurement.height + contentOffset.y >=
			contentSize.height - paddingToBottom;
	};


	return (
		<View style={{ flex: 1 }}>
			<ScrollView>
				<View style={GlobalStyle.formHeaderContentContainer}>
					<CustomInputComponent
						disabled
						label='Document ID'
						value={headerID}
					/>
					<CustomInputComponent
						disabled
						label='SAP Document ID'
						value={SAPDoc}
					/>
					<CustomInputComponent
						disabled
						label='Movement Type'
						value={MVT}
					/>
					<CustomInputComponent
						disabled
						label='Plant'
						value={plant}
					/>
					<CustomInputComponent
						disabled
						label='Requested By'
						value={createdBy}
					/>
					<CustomInputComponent
						isDate
						onDatePress={() => changeDate()}
						label='Posting Date'
						value={postingDate}
					/>
					{isShowDatePicker && (
						<DateTimePicker
							testID="dateTimePicker"
							value={new Date()}
							mode={mode}
							is24Hour={true}
							display="default"
							onChange={onChangeDate}
						// onCancel={setIsShowDatePicker(false)}
						/>)}
					<CustomInputComponent
						disabled
						label='Created Date'
						value={createdTime}
					/>
				</View>

				<View style={GlobalStyle.formContentContainer}>
					<Text style={GlobalStyle.formHeaderTitleText}>List of Material ({materialList.length})</Text>
					<View style={{ marginTop: 10 }}>
						<FlatList
							nestedScrollEnabled={true}
							style={{ maxHeight: Metrics.SCREEN_WIDTH * 2 }}
							onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}
							onScrollEndDrag={({ nativeEvent }) => {
								if (isCloseToBottom(nativeEvent) && materialList.length > filteredMaterialListIndex && isLoading === false) {
									loadOtherIndex()
								}
							}}
							scrollEventThrottle={100}
							refreshControl={(
								<RefreshControl
									refreshing={isLoading}
									onRefresh={initialLoad}
								/>
							)}
							data={filteredMaterialList}
							extraData={filteredMaterialList}
							keyExtractor={item => String(item.TR_TP_DETAIL_ID)}
							contentContainerStyle={GlobalStyle.viewContainer}
							// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
							onEndReachedThreshold={0.1}
							ListEmptyComponent={(
								<View style={{ flex: 1, alignItems: 'center', paddingTop: 50 }}>
									<View style={styles.illustContainerImage}>
										<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
										<View style={styles.titleTextContainer}>
											<Text style={styles.illustTextDesc}>Material is empty</Text>
										</View>
									</View>
								</View>
							)}
							renderItem={({ item, index }) => (
								<CardDisplay
									mvt={MVT}
									materialData={savedMaterial}
									data={item} />
							)}
						// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
						// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
						/>
					</View>
				</View>

				<View style={GlobalStyle.formContentContainer}>
					<View style={[GlobalStyle.formButtonContainer, { paddingHorizontal: 0 }]}>
						<CustomButton
							customColor={Colors.GREEN_LIGHT}
							onPress={() => handleTakePhoto()}
							label='Take Photo'
						/>
						{tempReceiverIDPhoto ? (
							<View style={styles.tempImageStyle}>
								<Image resizeMethod="resize" resizeMode='cover' source={{ uri: tempReceiverIDPhoto }} style={{ height: '100%', width: '100%' }} />
							</View>
						) : null}
					</View>
				</View>

				<View style={GlobalStyle.formContentContainer}>
					<CustomInputComponent
						isVertical
						label='Notes'
						isLabelBold
						value={catatan}
						placeholder='Notes'
						onChangeText={setCatatan}
					/>
				</View>

				<View style={GlobalStyle.formContentContainer}>
					<CustomInputComponent
						isVertical
						label='BOL (Optional)'
						isLabelBold
						value={BOL}
						placeholder='Bill Of Landing'
						onChangeText={setBOL}
					/>
				</View>

				<View style={[GlobalStyle.formButtonContainer, { flexDirection: "row" }]}>
					<CustomButton
						onPress={handleSubmitTP}
						isLoading={isLoading}
						label='Submit'
					/>
					<View style={{ width: 10 }}></View>
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						isLoading={isLoading}
						label='Back'
					/>
				</View>
			</ScrollView>
			<CustomModalConfirm
				title='Submit Transfer Posting'
				body='Are you sure want to submit Transfer Posting? Make sure your data are correct.'
				onConfirm={onSubmitTransferPosting}
				onCancel={() => setIsModalConfirmVisible(false)}
				isModalVisible={isModalConfirmVisible}
			/>
			<CustomModalCamera
				onPictureTaken={onPictureTaken}
				onCancel={() => setIsModalTakePhotoVisible(false)}
				isModalVisible={isModalTakePhotoVisible}
			/>
			<CustomToast ref={toastRef} />
		</View >
	)
}

export default DetailTransferPosting

const styles = StyleSheet.create({
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	illustContainerImage: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	illustStyle: {
		width: 200,
		height: 200,
		resizeMode: 'cover',
	},
	illustTextDesc: {
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontSize: 14,
		textAlign: 'center',
		marginTop: 20,
		color: Colors.BLACK,
	},
})