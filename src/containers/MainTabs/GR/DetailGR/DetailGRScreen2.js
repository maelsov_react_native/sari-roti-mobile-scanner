import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, View, Text, StyleSheet, FlatList, TouchableOpacity, Image, RefreshControl, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux';
import DateTimePicker from '@react-native-community/datetimepicker';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';

import { Colors, Fonts, Icons, Illustrations, Metrics } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { getGRDataSubmit, getPoNumber, getUserData, wait } from '../../../../globals/GlobalFunction';
import { modelTransactions } from '../../../../models/Transaction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomModalCamera from '../../../../components/CustomModalCamera';
import CustomButton from '../../../../components/CustomButton';
import CustomToast from '../../../../components/CustomToast';
import CustomModalConfirm from '../../../../components/CustomModalConfirm';

const CardDisplay = (props) => {
	const { data, materialData } = props
	const {
		TR_PO_DETAIL_ID,
		TR_PO_DETAIL_MATERIAL_NAME,
		TR_PO_DETAIL_UOM,
		TR_PO_DETAIL_QTY_ORDER,
		TR_PO_DETAIL_SLOC,
		TR_PO_DETAIL_PLANT_RCV,
		TR_PO_DETAIL_MATERIAL_CODE,
	} = data

	const isChecked = materialData.findIndex(e => e.poDetailId == TR_PO_DETAIL_ID) > -1

	const found = materialData.find(e => e.poDetailId == TR_PO_DETAIL_ID)

	const goToDetailMaterial = () => {
		if (isChecked) {
			Actions.detailMaterialGR({ poDetailNumber: TR_PO_DETAIL_ID, materialCode: TR_PO_DETAIL_MATERIAL_CODE, materialName: TR_PO_DETAIL_MATERIAL_NAME, storageLocation: found.storageLocation, totalQTY: TR_PO_DETAIL_QTY_ORDER, foto: found.photo, foto2: found.photoTemp, subexpiredDate: found.expiredDate, note: found.note, qty: found.qty, batch: found.batch, uom: found.batch, submited: true })
		}
		else {
			Actions.detailMaterialGR({ poDetailNumber: TR_PO_DETAIL_ID, materialCode: TR_PO_DETAIL_MATERIAL_CODE, materialName: TR_PO_DETAIL_MATERIAL_NAME, storageLocation: TR_PO_DETAIL_SLOC, totalQTY: TR_PO_DETAIL_QTY_ORDER, uom: TR_PO_DETAIL_UOM })
		}
	}
	return (
		<TouchableOpacity
			style={{ flexDirection: "row", paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: Colors.GRAY_DARK, marginBottom: 20 }}
			onPress={() => goToDetailMaterial()}>
			<View style={{ flex: 4 }}>
				<CustomInputComponent
					disabled
					label='Material'
					value={TR_PO_DETAIL_MATERIAL_NAME}
				/>
				<CustomInputComponent
					disabledTextInput
					suffix={TR_PO_DETAIL_UOM}
					label='Quantity'
					value={TR_PO_DETAIL_QTY_ORDER}
				/>
				<CustomInputComponent
					disabled
					label='Sloc'
					value={isChecked ? found.storageLocation : TR_PO_DETAIL_SLOC}
				/>
				<CustomInputComponent
					disabled
					label='Plant'
					value={TR_PO_DETAIL_PLANT_RCV}
				/>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				{isChecked && (
					<FontAwesome
						name='check'
						color={Colors.GREEN}
						size={16}
						style={{ position: "absolute", top: 5, right: 5, height: 20, width: 20 }}
					/>
				)
				}
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const DetailGRScreen2 = (props) => {
	const { lastUpdate } = props
	const [scrollPosition, setScrollPosition] = useState(0)
	const [isLoading, setIsLoading] = useState(false)
	const [submitedMaterialList, setSubmitedMaterialList] = useState([])
	const [poNumber, setPoNumber] = useState('')
	const [catatan, setCatatan] = useState('')
	const [BOL, setBOL] = useState('')
	const [mode, setMode] = useState('')
	const [isShowDatePicker, setIsShowDatePicker] = useState(false)
	const [materialList, setMaterialList] = useState([])
	const [filteredMaterialList, setFilteredMaterialList] = useState([])
	const [filteredMaterialListIndex, setFilteredMaterialListIndex] = useState("5")
	const [tempReceiverIDPhoto, setTempReceiverIDPhoto] = useState('')
	const [fotoBase64AbsensiList, setFotoBase64AbsensiList] = useState('')
	const [postingDate, setPostingDate] = useState('')
	const [isAdjusted, setIsAdjusted] = useState(false)
	const [isModalConfirmVisible, setIsModalConfirmVisible] = useState(false)
	const [isModalTakePhotoVisible, setIsModalTakePhotoVisible] = useState(false)
	const toastRef = useRef(null);

	useEffect(() => {
		getGRDataSubmit().then(result => {
			if (result != null) {
				setSubmitedMaterialList(result)
			}
		})
		getPoNumber().then(result => {
			setPoNumber(result)
			initialLoad(result)
		})
	}, [lastUpdate])

	const initialLoad = (data) => {
		setIsLoading(true)
		wait(2000).then(() => {
			let params_data = `gr_po_number=${data}`
			modelTransactions.getGRPODetail(params_data, res => {
				const { status, result } = res
				switch (status) {
					case 200:
						setMaterialList(result.data.detail_data)
						if (result.data.detail_data.length > 5) {
							setFilteredMaterialList(result.data.detail_data.slice(0, filteredMaterialListIndex))
						}
						else {
							setFilteredMaterialList(result.data.detail_data)
						}
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const loadOtherIndex = () => {
		setIsLoading(true)
		const index = parseInt(filteredMaterialListIndex) + 5
		if (index < materialList.length) {
			setFilteredMaterialListIndex(index)
			setFilteredMaterialList(materialList.slice(0, index))
			setIsLoading(false)
		}
		else if (index >= materialList.length) {
			setFilteredMaterialListIndex(materialList.length)
			setFilteredMaterialList(materialList.slice(0, materialList.length))
			setIsLoading(false)
		}
	}

	const handleTakePhoto = () => {
		setIsModalTakePhotoVisible(true)
	}

	const onPictureTaken = (cameraData) => {
		const {
			uri,
			base64
		} = cameraData

		setFotoBase64AbsensiList(base64)
		setTempReceiverIDPhoto(uri)
		setIsModalTakePhotoVisible(false)
	}

	const handleSubmitGR = () => {
		const { current } = toastRef
		if (tempReceiverIDPhoto === '') {
			current.showToast('warning', "Mohon ambil foto!")
		}
		else if (postingDate === '') {
			current.showToast('warning', "Mohon input tanggal posting!")
		}
		else if (catatan === '') {
			current.showToast('warning', "Mohon input catatan!")
		}
		else {
			const jumlahData = materialList.reduce((acc, cur) => {
				const found = submitedMaterialList.findIndex(e => e.poDetailId == cur.TR_PO_DETAIL_ID) > -1
				if (found) return acc + 1
				else acc
			}, 0)

			const goodData = jumlahData == materialList.length
			console.log(jumlahData)
			console.log(materialList.length)
			console.log(goodData)
			if (goodData) {
				setIsModalConfirmVisible(true)
			}
			else {
				current.showToast('warning', "Mohon submit material lainnya!")
			}
		}
	}

	const onSubmitGR = () => {
		getUserData().then(result => {
			const { current } = toastRef
			setIsLoading(true)
			const post_data = new FormData();
			post_data.append('po_number', poNumber)
			post_data.append('TR_GR_HEADER_PSTG_DATE', moment(postingDate).format('YYYY-MM-DD'))
			post_data.append('TR_GR_HEADER_IS_ADJUSTMENT', isAdjusted)
			post_data.append('TR_GR_HEADER_BOL', BOL)
			post_data.append('TR_GR_HEADER_RECIPIENT', result.user_initial_name)
			post_data.append('TR_GR_HEADER_TXT', catatan)
			post_data.append('login_user_id', result.user_id)
			post_data.append('login_plant_code', result.user_plant)
			post_data.append('GR_PHOTO', fotoBase64AbsensiList)
			for (let index = 0; index < submitedMaterialList.length; index++) {
				const element = submitedMaterialList[index];
				post_data.append('gr_materials[' + index + '][po_detail_id]', element.poDetailId)
				post_data.append('gr_materials[' + index + '][material_code]', element.materialCode)
				post_data.append('gr_materials[' + index + '][material_name]', element.materialName)
				post_data.append('gr_materials[' + index + '][batch]', element.batch)
				post_data.append('gr_materials[' + index + '][TR_GR_DETAIL_SLOC]', element.storageLocation)
				post_data.append('gr_materials[' + index + '][qty]', element.qty)
				post_data.append('gr_materials[' + index + '][uom]', element.uom)
				post_data.append('gr_materials[' + index + '][expired_date]', element.expiredDate)
				post_data.append('gr_materials[' + index + '][TR_GR_DETAIL_NOTES]', element.note)
				post_data.append('gr_materials[' + index + '][materialPhoto]', element.photo)
			}
			console.log(post_data)
			modelTransactions.GRSubmit(post_data, res => {
				const { status, result } = res
				console.log(res)
				switch (status) {
					case 200:
						setIsModalConfirmVisible(false)
						setIsLoading(true)
						wait(2000).then(() => {
							current.showToast('success', "Good Receipt has been submitted successfully!")
							wait(2000).then(() => {
								setIsLoading(false)
								Actions.gr()
							})
						})
						break;
					case 400:
						setIsModalConfirmVisible(false)
						setIsLoading(false)
						current.showToast('warning', "Data already Submitted, Transaction Cancelled")
						break;
					default:
						setIsModalConfirmVisible(false)
						setIsLoading(false)
						current.showToast('warning', `*${result.message}*`)
						break;
				}
			})
		})
	}

	const changeDate = () => {
		setIsShowDatePicker(true)
	}

	const onChangeDate = (event, selectedDate) => {
		const currentDate = selectedDate || date;
		console.log(currentDate)
		setIsShowDatePicker(false);
		setPostingDate(currentDate);
	};

	const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
		const paddingToBottom = 20;
		return layoutMeasurement.height + contentOffset.y >=
			contentSize.height - paddingToBottom;
	};

	return (
		<View style={{ flex: 1 }}>
			<ScrollView>
				<View style={GlobalStyle.formHeaderContentContainer}>
					<CustomInputComponent
						disabled
						label='PO Number'
						value={poNumber}
					/>
					<CustomInputComponent
						isDate
						onDatePress={() => changeDate()}
						label='Posting Date'
						value={postingDate}
					/>
					{isShowDatePicker && (
						<DateTimePicker
							testID="dateTimePicker"
							value={new Date()}
							mode={mode}
							is24Hour={true}
							display="default"
							onChange={onChangeDate}
						/>)}
					<CustomInputComponent
						isCheck
						label='Good Receipt Adjustment'
						value={isAdjusted}
						onValueChange={setIsAdjusted}
					/>
				</View>
				<View style={GlobalStyle.formContentContainer}>
					<View style={[GlobalStyle.formButtonContainer, { paddingHorizontal: 0 }]}>
						<CustomButton
							customColor={Colors.GREEN_LIGHT}
							onPress={() => handleTakePhoto()}
							label='Take Photo'
						/>
						{tempReceiverIDPhoto ? (
							<View style={styles.tempImageStyle}>
								<Image resizeMethod="resize" resizeMode='cover' source={{ uri: tempReceiverIDPhoto }} style={{ height: '100%', width: '100%' }} />
							</View>
						) : null}
					</View>
				</View>
				<View style={GlobalStyle.formContentContainer}>
					<Text style={GlobalStyle.formHeaderTitleText}>List of Material ({materialList.length})</Text>
					<View style={{ marginTop: 10, flex: 1 }}>
						<FlatList
							nestedScrollEnabled={true}
							style={{ maxHeight: Metrics.SCREEN_WIDTH*2 }}
							onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}
							onScrollEndDrag={({ nativeEvent }) => {
								if (isCloseToBottom(nativeEvent) && materialList.length > filteredMaterialListIndex && isLoading === false) {
									loadOtherIndex()
								}
							}}
							scrollEventThrottle={100}
							refreshControl={(
								<RefreshControl
									refreshing={isLoading}
									onRefresh={initialLoad}
								/>
							)}
							data={filteredMaterialList}
							extraData={filteredMaterialList}
							keyExtractor={item => String(item.TR_PO_DETAIL_ID)}
							contentContainerStyle={GlobalStyle.viewContainer}
							ListEmptyComponent={(
								<View style={{ flex: 1, alignItems: 'center' }}>
									<View style={styles.illustContainerImage}>
										<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
										<View style={styles.titleTextContainer}>
											<Text style={styles.illustTextDesc}>Material is empty</Text>
										</View>
									</View>
								</View>
							)}
							renderItem={({ item, index }) => (
								<CardDisplay
									materialData={submitedMaterialList}
									data={item} />
							)}
						// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
						// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
						/>
					</View>
				</View>

				<View style={GlobalStyle.formContentContainer}>
					<CustomInputComponent
						isVertical
						label='Notes'
						isLabelBold
						value={catatan}
						placeholder='Notes'
						onChangeText={setCatatan}
					/>
				</View>
				<View style={GlobalStyle.formContentContainer}>
					<CustomInputComponent
						isVertical
						label='BOL (Optional)'
						isLabelBold
						value={BOL}
						placeholder='Bill Of Landing'
						onChangeText={setBOL}
					/>
				</View>
				<View style={[GlobalStyle.formButtonContainer, { flexDirection: "row" }]}>
					<CustomButton
						onPress={handleSubmitGR}
						isLoading={isLoading}
						label='Submit'
					/>
					<View style={{ width: 10 }}></View>
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						isLoading={isLoading}
						label='Back'
					/>
				</View>
			</ScrollView>
			<CustomModalConfirm
				title='Submit Good Receipt'
				body='Are you sure want to submit Good Receipt? Make sure your data are correct.'
				onConfirm={onSubmitGR}
				onCancel={() => setIsModalConfirmVisible(false)}
				isModalVisible={isModalConfirmVisible}
			/>
			<CustomModalCamera
				onPictureTaken={onPictureTaken}
				onCancel={() => setIsModalTakePhotoVisible(false)}
				isModalVisible={isModalTakePhotoVisible}
			/>

			<CustomToast ref={toastRef} />
		</View >
	)
}

export default DetailGRScreen2

const styles = StyleSheet.create({
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	illustContainerImage: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	illustStyle: {
		width: 200,
		height: 200,
		resizeMode: 'cover',
	},
	illustTextDesc: {
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontSize: 14,
		textAlign: 'center',
		marginTop: 20,
		color: Colors.BLACK,
	},
})