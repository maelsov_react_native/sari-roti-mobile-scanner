import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, View, Text, StyleSheet, FlatList, TouchableOpacity, Image, RefreshControl } from 'react-native'
import { Actions } from 'react-native-router-flux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';

import { Colors, Fonts, Icons, Illustrations, Metrics } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { getGRDataSubmit, getPoNumber, wait } from '../../../../globals/GlobalFunction';
import { modelTransactions } from '../../../../models/Transaction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomButton from '../../../../components/CustomButton';
import CustomToast from '../../../../components/CustomToast';

const CardDisplay = (props) => {
	const { data, materialData } = props
	const {
		TR_PO_DETAIL_ID,
		TR_PO_DETAIL_UOM,
		TR_PO_DETAIL_MATERIAL_NAME,
		TR_PO_DETAIL_QTY_ORDER,
		TR_PO_DETAIL_SLOC,
		TR_PO_DETAIL_PLANT_RCV,
		TR_PO_DETAIL_MATERIAL_CODE,
	} = data

	const isChecked = materialData.findIndex(e => e.poDetailId == TR_PO_DETAIL_ID) > -1

	const found = materialData.find(e => e.poDetailId == TR_PO_DETAIL_ID)

	const goToDetailMaterial = () => {
		if (isChecked) {
			Actions.detailMaterialGR({ poDetailNumber: TR_PO_DETAIL_ID, materialCode: TR_PO_DETAIL_MATERIAL_CODE, materialName: TR_PO_DETAIL_MATERIAL_NAME, storageLocation: found.storageLocation, totalQTY: TR_PO_DETAIL_QTY_ORDER, foto: found.photo, foto2: found.photoTemp, subexpiredDate: found.expiredDate, note: found.note, qty: found.qty, batch: found.batch, uom: found.batch, submited: true })
		}
		else {
			Actions.detailMaterialGR({ poDetailNumber: TR_PO_DETAIL_ID, materialCode: TR_PO_DETAIL_MATERIAL_CODE, materialName: TR_PO_DETAIL_MATERIAL_NAME, storageLocation: TR_PO_DETAIL_SLOC, totalQTY: TR_PO_DETAIL_QTY_ORDER, uom: TR_PO_DETAIL_UOM })
		}
	}
	return (
		<TouchableOpacity
			style={{ flexDirection: "row", paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: Colors.GRAY_DARK, marginBottom: 20 }}
			onPress={() => goToDetailMaterial()}>
			<View style={{ flex: 4 }}>
				<CustomInputComponent
					disabled
					label='Material'
					value={TR_PO_DETAIL_MATERIAL_NAME}
				/>
				<CustomInputComponent
					disabledTextInput
					suffix={TR_PO_DETAIL_UOM}
					label='Quantity'
					value={TR_PO_DETAIL_QTY_ORDER}
				/>
				<CustomInputComponent
					disabled
					label='Sloc'
					value={isChecked ? found.storageLocation : TR_PO_DETAIL_SLOC}
				/>
				<CustomInputComponent
					disabled
					label='Plant'
					value={TR_PO_DETAIL_PLANT_RCV}
				/>
			</View>
			<View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
				{isChecked && (
					<FontAwesome
						name='check'
						color={Colors.GREEN}
						size={16}
						style={{ position: "absolute", top: 5, right: 5, height: 20, width: 20 }}
					/>
				)
				}
				<Image resizeMethod="resize" source={Icons.iconChevronRight} style={{ height: 15, width: 15 }} />
			</View>
		</TouchableOpacity>
	)
}

const DetailGRScreen = (props) => {
	const { lastUpdate } = props
	const [scrollPosition, setScrollPosition] = useState(0)
	const [isLoading, setIsLoading] = useState(false)

	const [materialList, setMaterialList] = useState([])
	const [submitedMaterialList, setSubmitedMaterialList] = useState([])
	const [filteredMaterialList, setFilteredMaterialList] = useState([])
	const [filteredMaterialListIndex, setFilteredMaterialListIndex] = useState("5")

	const [poNumber, setPoNumber] = useState('')
	const [createdDate, setCreatedDate] = useState('')
	const [plant, setPlant] = useState('')
	const [vendorName, setVendorName] = useState('')

	const toastRef = useRef(null);

	useEffect(() => {
		if (lastUpdate) {
			getGRDataSubmit().then(result => {
				if (result != null) {
					setSubmitedMaterialList(result)
				}
			})
			getPoNumber().then(result => {
				setPoNumber(result)
				initialLoad(result)
			})
		}
	}, [lastUpdate])

	const initialLoad = (data) => {
		setIsLoading(true)
		wait(2000).then(() => {
			let params_data = `gr_po_number=${data}`
			modelTransactions.getGRPODetail(params_data, res => {
				const { status, result } = res
				console.log(result.data)
				switch (status) {
					case 200:
						setMaterialList(result.data.detail_data)
						if (result.data.detail_data.length > 5) {
							setFilteredMaterialList(result.data.detail_data.slice(0, filteredMaterialListIndex))
						}
						else {
							setFilteredMaterialList(result.data.detail_data)
						}
						setCreatedDate(moment(result.data.header_data.TR_PO_HEADER_CREATED_TIMESTAMP).format('YYYY-MM-DD'))
						setVendorName(result.data.header_data.TR_PO_HEADER_VENDOR)
						setPlant(result.data.header_data.TR_PO_HEADER_SUP_PLANT)
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const loadOtherIndex = () => {
		setIsLoading(true)
		const index = parseInt(filteredMaterialListIndex) + 5
		if (index < materialList.length) {
			setFilteredMaterialListIndex(index)
			setFilteredMaterialList(materialList.slice(0, index))
			setIsLoading(false)
		}
		else if (index >= materialList.length) {
			setFilteredMaterialListIndex(materialList.length)
			setFilteredMaterialList(materialList.slice(0, materialList.length))
			setIsLoading(false)
		}
	}

	const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
		const paddingToBottom = 20;
		return layoutMeasurement.height + contentOffset.y >=
			contentSize.height - paddingToBottom;
	};

	const createGoodReceipt = () => {
		Actions.detailGR2()
	}

	return (
		<View style={{ flex: 1 }}>
			<ScrollView>
				<View style={GlobalStyle.formHeaderContentContainer}>
					<CustomInputComponent
						disabled
						label='PO Number'
						value={poNumber}
					/>
					<CustomInputComponent
						disabled
						label='Vendor name'
						value={vendorName}
					/>
					<CustomInputComponent
						disabled
						label='Movement Type'
						value="101"
					/>
					<CustomInputComponent
						disabled
						label='Plant'
						value={plant}
					/>
					<CustomInputComponent
						disabled
						label='GL Account'
						value="-"
					/>
					<CustomInputComponent
						disabled
						label='Cost Center'
						value="-"
					/>
					<CustomInputComponent
						disabled
						label='Created Date'
						value={createdDate}
					/>
				</View>
				<View style={GlobalStyle.formButtonContainer}>
					<CustomButton
						customColor={Colors.BLUE_LIGHT}
						onPress={() => createGoodReceipt()}
						label='Create Good Receipt'
					/>
				</View>
				<View style={GlobalStyle.formContentContainer}>
					<Text style={GlobalStyle.formHeaderTitleText}>List of Material ({materialList.length})</Text>
					<View style={{ marginTop: 10 }}>
						<FlatList
							nestedScrollEnabled={true}
							style={{ maxHeight: Metrics.SCREEN_WIDTH * 2 }}
							onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}
							onScrollEndDrag={({ nativeEvent }) => {
								if (isCloseToBottom(nativeEvent) && materialList.length > filteredMaterialListIndex && isLoading === false) {
									loadOtherIndex()
								}
							}}
							scrollEventThrottle={100}
							refreshControl={(
								<RefreshControl
									refreshing={isLoading}
									onRefresh={initialLoad}
								/>
							)}
							data={filteredMaterialList}
							extraData={filteredMaterialList}
							keyExtractor={item => String(item.TR_PO_DETAIL_ID)}
							contentContainerStyle={GlobalStyle.viewContainer}
							// onEndReached={listKualitasAirDataMalam.length > 9 && isLoadingKualitasAirMalam == false && isLoadingPullKualitasAirMalam == false ? () => this.fetchApiKualitasAirMalam("endReached") : null}
							onEndReachedThreshold={0.1}
							ListEmptyComponent={(
								<View style={{ flex: 1, alignItems: 'center' }}>
									<View style={styles.illustContainerImage}>
										<Image style={styles.illustStyle} source={Illustrations.illustrationEmpty} />
										<View style={styles.titleTextContainer}>
											<Text style={styles.illustTextDesc}>Material is empty</Text>
										</View>
									</View>
								</View>
							)}
							renderItem={({ item, index }) => (
								<CardDisplay
									materialData={submitedMaterialList}
									data={item} />
							)}
						// ListEmptyComponent={<EmptyStateUI title="Data kualitas air (malam) tidak ditemukan" />}
						// ListFooterComponent={this.renderFooterFlatListMalam.bind(this)}
						/>
					</View>
				</View>

				<View style={[GlobalStyle.formButtonContainer, { flexDirection: "row" }]}>
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						isLoading={isLoading}
						label='Back'
					/>
				</View>
			</ScrollView>
			<CustomToast ref={toastRef} />
		</View>
	)
}

export default DetailGRScreen

const styles = StyleSheet.create({
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	illustContainerImage: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	illustStyle: {
		width: 200,
		height: 200,
		resizeMode: 'cover',
	},
	illustTextDesc: {
		fontFamily: Fonts.ROBOTO_REGULAR,
		fontSize: 14,
		textAlign: 'center',
		marginTop: 20,
		color: Colors.BLACK,
	},
})