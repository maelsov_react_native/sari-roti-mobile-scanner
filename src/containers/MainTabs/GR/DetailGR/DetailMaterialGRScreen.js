import React, { useEffect, useRef, useState } from 'react'
import { ScrollView, View, StyleSheet, Image } from 'react-native'
import { Actions } from 'react-native-router-flux';

import { Colors, Fonts, StorageKeys } from '../../../../globals/GlobalConfig';
import GlobalStyle from '../../../../globals/GlobalStyle';
import { getGRDataSubmit, inputValidation, wait } from '../../../../globals/GlobalFunction';

import CustomInputComponent from '../../../../components/CustomInputComponent'
import CustomModalCamera from '../../../../components/CustomModalCamera';
import CustomButton from '../../../../components/CustomButton';
import CustomToast from '../../../../components/CustomToast';
import CustomModalConfirm from '../../../../components/CustomModalConfirm';
import DateTimePicker from '@react-native-community/datetimepicker';
import { modelTransactions } from '../../../../models/Transaction';
import AsyncStorage from '@react-native-community/async-storage';

const DetailMaterialGRScreen = (props) => {
	const { subexpiredDate, note, qty, batch, submited, foto2, uom } = props
	const [scrollPosition, setScrollPosition] = useState(0)
	const [batchList, setBatchList] = useState([])
	const [storageLocation, setStorageLocation] = useState([])

	const [expiredDate, setExpiredDate] = useState(submited ? subexpiredDate : '')
	const [catatan, setCatatan] = useState(submited ? note : '')
	const [receivedQuantity, setReceivedQuantity] = useState(submited ? qty : '')
	const [qtyLeft, setQTYLeft] = useState('')
	const [UOM, setUOM] = useState(uom)
	const [selectedBatch, setSelectedBatch] = useState(submited ? batch : '')
	const [selectedSLOC, setSelectedSLOC] = useState(props.storageLocation)

	const [materialCode, setMaterialCode] = useState(props.materialCode)
	const [materialName, setMaterialName] = useState(props.materialName)
	const [totalQTY, setTotalQTY] = useState(props.totalQTY)
	const [poDetailId, setPoDetailId] = useState(props.poDetailNumber)
	const [mode, setMode] = useState('')

	const [tempReceiverIDPhoto, setTempReceiverIDPhoto] = useState(submited ? foto2 : '')
	const [fotoBase64AbsensiList, setFotoBase64AbsensiList] = useState('')

	const [isLoading, setIsLoading] = useState(false)
	const [isModalConfirmVisible, setIsModalConfirmVisible] = useState(false)
	const [isModalTakePhotoVisible, setIsModalTakePhotoVisible] = useState(false)
	const [isShowDatePicker, setIsShowDatePicker] = useState(false)

	const toastRef = useRef(null);

	useEffect(() => {
		initialLoad()
	}, [])

	const initialLoad = () => {
		wait(2000).then(() => {
			let params_data = `po_detail_id=${poDetailId}`
			modelTransactions.getGRMaterialDetail(params_data, res => {
				const { status, result } = res
				console.log(res)
				console.log(result.data.batch_list)
				switch (status) {
					case 200:
						setQTYLeft(result.data.qty_left)
						setBatchList(result.data.batch_list)
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			let params_data2 = ``
			modelTransactions.getSLOC(params_data2, res => {
				const { status, result } = res
				switch (status) {
					case 200:
						let foundStorageLoc = result.data.findIndex(e => e.id == props.storageLocation) > -1
						if (foundStorageLoc) {
							let storageLoc = result.data.find(e => e.id == props.storageLocation)
							setSelectedSLOC(storageLoc.id)
						}
						wait(2000).then(() => {
						setStorageLocation(result.data)
					})
						break;
					case 500:
						current.showToast('error', "Connection not available")
						break;
					default:
						current.showToast('error', "Connection not available")
						break;
				}
			})
			setIsLoading(false)
		})
	}

	const handleSubmitMaterial = () => {
		const { current } = toastRef

		if (receivedQuantity == '') {
			current.showToast('warning', "Mohon input jumlah barang diterima!")
		}
		else if (expiredDate == '') {
			current.showToast('warning', "Mohon input tanggal kadaluarsa!")
		}
		else if (catatan == '') {
			current.showToast('warning', "Mohon input catatan!")
		}
		// else if (tempReceiverIDPhoto == '') {
		// 	current.showToast('warning', "Mohon ambil foto!")
		// }
		else if (selectedSLOC == '') {
			current.showToast('warning', "Mohon pilih Sloc!")
		}
		else if (parseFloat(receivedQuantity) == 0) {
			current.showToast('warning', "Barang diterima tidak boleh 0!")
		}
		else if (selectedBatch == '' && batchList.length != 0) {
			current.showToast('warning', "Mohon pilih Batch!")
		}
		else if (parseFloat(receivedQuantity) > parseFloat(qtyLeft)) {
			current.showToast('warning', "Barang diterima lebih besar dari sisa barang!")
		}
		else {
			setIsModalConfirmVisible(true)
		}
	}

	const onSubmitMaterial = () => {
		getGRDataSubmit().then(result => {
			const data = {
				poDetailId: poDetailId,
				batch: selectedBatch,
				materialCode: materialCode,
				materialName: materialName,
				uom: UOM,
				storageLocation: selectedSLOC,
				qty: receivedQuantity,
				expiredDate: expiredDate,
				photo: fotoBase64AbsensiList,
				note: catatan,
				photoTemp: tempReceiverIDPhoto,
			};
			if (result != null) {
				let grData = [...result, data]
				AsyncStorage.setItem(StorageKeys.GR_DATA_SUBMIT, JSON.stringify(grData));
			}
			else {
				AsyncStorage.setItem(StorageKeys.GR_DATA_SUBMIT, JSON.stringify([data]));
			}
		});
		setIsModalConfirmVisible(false)
		Actions.pop()
	}

	const handleTakePhoto = () => {
		setIsModalTakePhotoVisible(true)
	}

	const onPictureTaken = (cameraData) => {
		const {
			uri,
			base64
		} = cameraData

		setFotoBase64AbsensiList(base64)
		setTempReceiverIDPhoto(uri)
		setIsModalTakePhotoVisible(false)
	}


	const changeDate = () => {
		setIsShowDatePicker(true)
	}

	const onChangeDate = (event, selectedDate) => {
		const currentDate = selectedDate || date;
		console.log(currentDate)
		setIsShowDatePicker(false);
		setExpiredDate(currentDate);
	};


	return (
		<View style={{ flex: 1 }}>
			<ScrollView
				onMomentumScrollEnd={e => setScrollPosition(e.nativeEvent.contentOffset.y)}>
				<View style={GlobalStyle.formHeaderContentContainer}>
					{!submited ?
						<CustomInputComponent
							isOption
							// isCustomOption
							label='Batch'
							optionList={batchList}
							isOptionIncludeValue={true}
							onValueChange={setSelectedBatch}
							value={selectedBatch}
						/>
						:
						<CustomInputComponent
							disabled
							isOption
							// isCustomOption
							label='Batch'
							optionList={batchList}
							isOptionIncludeValue={true}
							onValueChange={setSelectedBatch}
							value={selectedBatch}
						/>
					}
					<CustomInputComponent
						disabled
						label='Material Code'
						value={materialCode}
					/>
					<CustomInputComponent
						disabled
						label='Material Name'
						value={materialName}
					/>

					{!submited ?
						<CustomInputComponent
							isOptionSloc
							label='Storage Location'
							optionList={storageLocation}
							onValueChange={setSelectedSLOC}
							value={selectedSLOC}
						/>
						:
						<CustomInputComponent
							disabled
							isOptionSloc
							// isCustomOption
							label='Storage Location'
							optionList={storageLocation}
							onValueChange={setSelectedSLOC}
							value={selectedSLOC}
						/>
					}
					<CustomInputComponent
						disabledTextInput
						label='Total Quantity'
						suffix={UOM}
						value={totalQTY}
					/>
					<CustomInputComponent
						disabledTextInput
						label='Remaining Unreceived Quantity'
						suffix={UOM}
						value={qtyLeft}
					/>
					{submited ?
						<>
							<CustomInputComponent
								disabledTextInput
								label='Received Quantity'
								suffix={UOM}
								value={receivedQuantity}
								placeholder='Input material quantity'
								onChangeText={inputValidation(setReceivedQuantity, 'number')}
								keyboardType='numeric'
							/>
							<CustomInputComponent
								disabled
								isDate
								onDatePress={() => changeDate()}
								label='Expired Date'
								value={expiredDate}
							/>
							<View style={GlobalStyle.formContentContainer}>
								<CustomInputComponent
									disabled
									isVertical
									label='Notes'
									isLabelBold
									value={catatan}
									placeholder='Notes'
									onChangeText={setCatatan}
								/>
							</View>
						</>
						:
						<>
							<CustomInputComponent
								label='Received Quantity'
								suffix={UOM}
								value={receivedQuantity}
								placeholder='Input material quantity'
								onChangeText={inputValidation(setReceivedQuantity, 'number')}
								keyboardType='numeric'
							/>
							<CustomInputComponent
								isDate
								onDatePress={() => changeDate()}
								label='Expired Date'
								value={expiredDate}
							/>
							{isShowDatePicker && (
								<DateTimePicker
									testID="dateTimePicker"
									value={new Date()}
									mode={mode}
									is24Hour={true}
									display="default"
									onChange={onChangeDate}
								/>)}
							<View style={GlobalStyle.formContentContainer}>
								<CustomInputComponent
									isVertical
									label='Notes'
									isLabelBold
									value={catatan}
									placeholder='Notes'
									onChangeText={setCatatan}
								/>
							</View>
						</>
					}
				</View>
				<View style={GlobalStyle.formContentContainer}>
					<View style={[GlobalStyle.formButtonContainer, { paddingHorizontal: 0 }]}>
						{!submited &&
							<CustomButton
								customColor={Colors.GREEN_LIGHT}
								onPress={() => handleTakePhoto()}
								label='Take Photo'
							/>
						}
						{tempReceiverIDPhoto || submited ? (
							<View style={styles.tempImageStyle}>
								<Image resizeMethod="resize" resizeMode='cover' source={{ uri: tempReceiverIDPhoto }} style={{ height: '100%', width: '100%' }} />
							</View>
						) : null}
					</View>
				</View>
				<View style={[GlobalStyle.formButtonContainer, { flexDirection: "row" }]}>
					{!submited &&
						<>
							<CustomButton
								onPress={handleSubmitMaterial}
								isLoading={isLoading}
								label='Save'
							/>
							<View style={{ width: 10 }}></View>
						</>
					}
					<CustomButton
						customColor={Colors.RED}
						onPress={() => Actions.pop()}
						label='Back'
					/>
				</View>
			</ScrollView>
			<CustomModalCamera
				onPictureTaken={onPictureTaken}
				onCancel={() => setIsModalTakePhotoVisible(false)}
				isModalVisible={isModalTakePhotoVisible}
			/>
			<CustomModalConfirm
				title='Save Material'
				body='Are you sure want to save material? Make sure your data is correct.'
				onConfirm={onSubmitMaterial}
				onCancel={() => setIsModalConfirmVisible(false)}
				isModalVisible={isModalConfirmVisible}
			/>
			<CustomToast ref={toastRef} />
		</View>
	)
}

export default DetailMaterialGRScreen

const styles = StyleSheet.create({
	tempImageStyle: {
		width: '100%',
		aspectRatio: 1,
		marginTop: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: Colors.GRAY,
		borderStyle: 'dashed',
		overflow: 'hidden'
	},
	buttonContainer: {
		flex: 1,
		height: 36,
		flexDirection: 'row',
		borderRadius: 6,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
})