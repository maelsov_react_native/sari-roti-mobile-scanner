import React, { useRef, useState, useEffect } from 'react'
import {
	StyleSheet,
	Text,
	View,
	Image,
	TouchableOpacity,
	TextInput,
	Animated,
	PermissionsAndroid,
	StatusBar
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage'

import GlobalStyle from '../../../globals/GlobalStyle';
import { Colors, DefaultErrorMessage, Fonts, Icons, Illustrations, Metrics, StorageKeys, Version } from '../../../globals/GlobalConfig';
import { isNullInteger, isNullString, validateEmail, wait } from '../../../globals/GlobalFunction';
import { modelAuthentication } from '../../../models/Authentication';

import CustomModalError from '../../../components/CustomModalError';
import CustomToast from '../../../components/CustomToast';
import CustomButtonLogin from '../../../components/CustomButtonLogin';

const LoginScreen = () => {
	const [userName, setUserName] = useState('')
	const [password, setPassword] = useState('')

	const [isModalErrorVisible, setIsModalErrorVisible] = useState(false)
	const [isLoading, setIsLoading] = useState(false)
	const [isVisiblePassword, setIsVisiblePassword] = useState(false)

	const toastRef = useRef(null);
	const passwordInputRef = useRef(null);

	const [animatedImage] = useState(new Animated.Value(0))

	useEffect(() => {
		requestPermissions()
		Animated.loop(
			Animated.sequence([
				Animated.timing(
					animatedImage,
					{
						toValue: 1,
						duration: 15000,
						useNativeDriver: true
					}
				),
				Animated.delay(2000),
				Animated.timing(
					animatedImage,
					{
						toValue: 0,
						duration: 15000,
						useNativeDriver: true
					}
				)
			])).start()
	})

	const requestPermissions = () => {
		PermissionsAndroid.requestMultiple([
			PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
			PermissionsAndroid.PERMISSIONS.CAMERA
		])
			.then(res => {
				if (Object.values(res).every((result) => result === 'granted')) {

				}
				else if (Object.values(res).some((result) => result === 'never_ask_again')) {
					requestPermissions()
				}
				else {
					requestPermissions()
				}
			})
	}

	const handleInputChange = (field) => (value) => {
		switch (field) {
			case 'userName':
				setUserName(value)
				break;
			case 'password':
				setPassword(value)
				break;
			default:
				break;
		}
	}

	const toggleShowPassword = () => setIsVisiblePassword(!isVisiblePassword)

	const hideModal = () => {
		setIsModalErrorVisible(false)
		setIsLoading(false)
	}

	const handleLogin = () => {
		const { current } = toastRef
		if (!userName && !password) {
			current.showToast('warning', "Please input your username and password!")
		}
		else if (!userName) {
			current.showToast('warning', "Please input your username!")
		}
		else if (!password) {
			current.showToast('warning', "Please input your password!")
		}
		else if (!validateEmail(userName)) {
			current.showToast('warning', "Please input a valid email address!")
		}
		else {
			onLogin()
		}
	}

	const onLogin = () => {
		const { current } = toastRef
		setIsLoading(true)
		modelAuthentication.login(userName, password, async res => {
			const { status, result } = res
			console.log(res)
			switch (status) {
				case 200:
					current.showToast('success', "Login Sukses")
					const user_data = {
						user_id: result.data.user_id,
						user_initial_name: result.data.user_initial_name,
						user_role: result.data.user_role,
						user_plant: result.data.plant
					}
					AsyncStorage.setItem(StorageKeys.USER_DATA, JSON.stringify(user_data), () => {
						AsyncStorage.setItem(StorageKeys.CUSTOMER_TOKEN, result.data.token, () => {
							wait(2000).then(() => {
								setIsLoading(false)
								// setUserName('')
								// setPassword('')
								Actions.tabBar()
							})
						})
					})
					break;
				case 400:
					setIsLoading(false)
					current.showToast('warning', `${result.message}`)
					break;
				default:
					setIsLoading(false)
					current.showToast('warning', `*${result.message}* Login Gagal`)
					break;
			}
		})
			.catch(err => {
				console.log(err)
				setIsLoading(false)
				current.showToast('error', DefaultErrorMessage)
			})
	}

	const animatedScale = animatedImage.interpolate({
		inputRange: [0, 1],
		outputRange: [-40, 40]
	});

	return (
		<View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
			<View style={[GlobalStyle.viewContainer, { height: Metrics.SCREEN_HEIGHT - StatusBar.currentHeight }]}>
				<View style={{ alignItems: "center", marginBottom: 20 }}>
					<Image resizeMethod="resize" source={Icons.iconLogoSariRoti} style={[styles.imageLogo, { width: 200 }]} />
				</View>
				<Text style={GlobalStyle.blackTitleText}>Login</Text>
				<Text style={GlobalStyle.blackContentText}>Input your username and password{'\n'}to start working</Text>

				<View style={styles.inputContainer}>
					<View style={styles.inputIconContainer}>
						<Image
							style={{ height: 28, width: 28, tintColor: Colors.GREEN_DARK }}
							source={Icons.iconPerson}
							resizeMode='contain'
						/>
					</View>
					<View style={{ flex: 1, justifyContent: 'center' }}>
						<TextInput
							style={styles.inputText}
							placeholderTextColor={`${Colors.GREEN_DARK}`}
							placeholder="Username"
							value={userName}
							selectionColor={Colors.GREEN_DARK}
							returnKeyType='next'
							onSubmitEditing={() => passwordInputRef.current.focus()}
							onChangeText={handleInputChange("userName")} />
					</View>
				</View>
				<View style={styles.inputContainer}>
					<View style={styles.inputIconContainer}>
						<Image
							style={{ height: 28, width: 28, tintColor: Colors.GREEN_DARK }}
							source={Icons.iconLock}
							resizeMode='contain'
						/>
					</View>
					<View style={{ flex: 1, justifyContent: 'center' }}>
						<TextInput
							ref={passwordInputRef}
							style={styles.inputText}
							placeholderTextColor={`${Colors.GREEN_DARK}`}
							placeholder="Password"
							autoCapitalize='none'
							secureTextEntry={!isVisiblePassword}
							value={password}
							selectionColor={Colors.GREEN_DARK}
							onChangeText={handleInputChange("password")} />
					</View>
					<TouchableOpacity onPress={toggleShowPassword} style={styles.inputIconContainer}>
						<FontAwesome name={isVisiblePassword ? "eye" : "eye-slash"} size={24} color={Colors.GREEN_DARK} />
					</TouchableOpacity>
				</View>
				<View style={{ marginTop: 30 }}>
					<CustomButtonLogin
						onPress={handleLogin}
						isLoading={isLoading}
						label='Login'
					/>
				</View>
				<Text style={styles.versionText}>v {Version}</Text>
				{/* <View style={styles.illustrationContainer}>
					<Text style={styles.versionText}>v {Version}</Text>
					<LinearGradient
						colors={[Colors.WHITE, '#FFF0']}
						style={{ position: 'absolute', top: 0, zIndex: 1, height: 40, width: '100%' }}
					/>
					<Animated.Image
						resizeMethod="resize"
						resizeMode='contain'
						source={Illustrations.illustrationLogin}
						style={[{ height: '100%', width: '100%', opacity: 0.8 }, { transform: [{ translateX: animatedScale }, { scaleX: 1.25 }] }]} />
				</View> */}
			</View>
			<CustomModalError
				title='Informasi'
				body='Koneksi internet tidak tersedia.'
				onCancel={hideModal}
				isModalVisible={isModalErrorVisible}
			/>
			<CustomToast ref={toastRef} />
		</View>
	)
}

export default LoginScreen

const styles = StyleSheet.create({
	illustrationContainer: {
		position: 'absolute',
		bottom: 0,
		width: Metrics.SCREEN_WIDTH,
		aspectRatio: 2.34,
		alignItems: 'center'
	},
	illustrationImage: {
		flex: 1,
		opacity: 0.6
	},
	inputContainer: {
		flexDirection: "row",
		backgroundColor: Colors.GRAY_LIGHT,
		height: 64,
		marginVertical: 10,
		alignItems: "center",
		borderRadius: 12,
		borderWidth: 1,
		borderColor: Colors.GRAY_DARK,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	inputIconContainer: {
		height: '100%',
		aspectRatio: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	inputText: {
		...GlobalStyle.textFontSize18,
		color: Colors.DARK,
		letterSpacing: 1,
		fontFamily: Fonts.ROBOTO_REGULAR
	},
	versionText: {
		marginTop: 50,
		...GlobalStyle.blackTitleText,
		...GlobalStyle.textFontSize16,
		textAlign: "center",
		zIndex: 2
	},
	imageLogo: {
		width: 120,
		height: 120,
		resizeMode: "contain"
	}
});