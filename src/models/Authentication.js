
import { APIList } from '../globals/APIConfig'
import fetchNoCache from '../libraries/fetchNoCache'

export const modelAuthentication = {
    login: (user_name, password, update) => {
        const post_data = new FormData
        post_data.append('email', user_name)
        post_data.append('password', password)

        return fetchNoCache(update, APIList.API_LOGIN, 'POST', '', post_data)
    },
    logout: (user_id, update) => {
        const post_data = new FormData
        post_data.append('user_id', user_id)

        return fetchNoCache(update, APIList.API_LOGOUT, 'POST', '', post_data)
    },
}