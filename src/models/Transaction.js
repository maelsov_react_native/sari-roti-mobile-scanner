
import { APIList } from '../globals/APIConfig'
import fetchNoCache from '../libraries/fetchNoCache'

export const modelTransactions = {
    //Good Receive
    getGRPOView: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GR_PO, 'GET', true, '', params_data)
    },
    getGRPODetail: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GR_PO_DETAIL, 'GET', true, '', params_data)
    },
    getGRMaterial: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GR_PO_MATERIAL, 'GET', true, '', params_data)
    },
    getGRMaterialDetail: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GR_MATERIAL_INFO, 'GET', true, '', params_data)
    },
    GRSubmit: (post_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GR_SUBMIT, 'POST', true, post_data)
    },
    //Good Issue
    getGIPOView: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GI_PO, 'GET', true, '', params_data)
    },
    getGIPODetail: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GI_PO_DETAIL, 'GET', true, '', params_data)
    },
    getGIMaterialDetail: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GI_MATERIAL_INFO, 'GET', true, '', params_data)
    },
    getGIScanQR: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GI_CHECK_QR, 'GET', true, '', params_data)
    },
    GISubmit: (post_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GI_SUBMIT, 'POST', true, post_data)
    },
    //Transfer Posting
    getTPView: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_TP_VIEW, 'GET', true, '', params_data)
    },
    getTPDetail: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_TP_DETAIL, 'GET', true, '', params_data)
    },
    getTPScanQR: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_GI_CHECK_QR_TP, 'GET', true, '', params_data)
    },
    TPSubmit: (post_data, update) => {
        return fetchNoCache(update, APIList.API_SUBMIT_TP, 'POST', true, post_data)
    },

    //Stock Opname
    getPIDView: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_PID_VIEW, 'GET', true, '', params_data)
    },
    getPIDDetail: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_PID_DETAIL, 'GET', true, '', params_data)
    },
    getPIDDetailMaterial: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_PID_DETAIL_MATERIAL, 'GET', true, '', params_data)
    },
    getPIDScanQR: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_PID_CHECK_QR, 'GET', true, '', params_data)
    },
    PIDSubmit: (post_data, update) => {
        return fetchNoCache(update, APIList.API_SUBMIT_PID, 'POST', true, post_data)
    },

    //SLOC
    getSLOC: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GET_SLOC, 'GET', true, '', params_data)
    },

    //History
    //GR
    getGRHistory: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GR_HEADER_HISTORY, 'GET', true, '', params_data)
    },
    getGRDetailHistory: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GR_DETAIL_HISTORY, 'GET', true, '', params_data)
    },
    //GI
    getGIHistory: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GI_HEADER_HISTORY, 'GET', true, '', params_data)
    },
    getGIDetailHistory: (params_data, update) => {
        return fetchNoCache(update, APIList.API_GI_DETAIL_HISTORY, 'GET', true, '', params_data)
    },
    //TP
    getTPHistory: (params_data, update) => {
        return fetchNoCache(update, APIList.API_TP_HEADER_HISTORY, 'GET', true, '', params_data)
    },
    getTPDetailHistory: (params_data, update) => {
        return fetchNoCache(update, APIList.API_TP_DETAIL_HISTORY, 'GET', true, '', params_data)
    },
    //PID
    getPIDHistory: (params_data, update) => {
        return fetchNoCache(update, APIList.API_PID_HEADER_HISTORY, 'GET', true, '', params_data)
    },
    getPIDDetailHistory: (params_data, update) => {
        return fetchNoCache(update, APIList.API_PID_DETAIL_HISTORY, 'GET', true, '', params_data)
    },
    getPIDDetailMaterialHistory: (params_data, update) => {
        return fetchNoCache(update, APIList.API_PID_DETAIL_MATERIAL_HISTORY, 'GET', true, '', params_data)
    },

    //REPORT
    ReportSubmit: (post_data, update) => {
        return fetchNoCache(update, APIList.API_REPORT_SUBMIT, 'POST', true, post_data)
    },
}