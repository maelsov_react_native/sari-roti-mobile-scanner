
import { APIList } from '../globals/APIConfig'
import fetchNoCache from '../libraries/fetchNoCache'

export const modelIntegration = {
    uploadData: (jsonData, update) => {
        const post_data = new FormData
        post_data.append('ms_data', jsonData)

        return fetchNoCache(update, APIList.API_UPLOAD, 'POST', '', post_data)
    }
}