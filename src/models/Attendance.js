
import { APIList } from '../globals/APIConfig'
import fetchNoCache from '../libraries/fetchNoCache'

export const modelAttendance = {
    retrieveWorkPlan: (chosenSubLocation, update) => {
        const post_data = new FormData
        post_data.append('sub_location_code', chosenSubLocation)

        return fetchNoCache(update, APIList.API_RETRIEVE_WORK_PLAN, 'POST', '', post_data)
    }
}