// const BASE_URL_DEV = 'http://206.189.81.185/monitoring-system/api/v1_1/'
const BASE_URL_DEV = 'http://178.128.114.224/DEV/api/v1_1/'
const BASE_URL_QAS = 'http://209.97.175.181/QAS/'
const BASE_URL_RAW = 'http://209.97.175.181'
const BASE_URL_LOCAL = 'localhost/SariRotiMobileBarcodeWeb/api/'
const BASE_URL_DEVELOP = 'http://128.199.239.247/SariRotiMobileBarcodeWeb/api/'
const BASE_URL_DEVELOP2 = 'http://172.27.100.6/DEV/api/'
const BASE_URL_PRODUCTION = 'https://rmwms.sariroti.com/api/'

//Toggle to change between DEV and PRD
const BASE_URL = BASE_URL_PRODUCTION
const APIRoot = {
    ROOT_AUTHENTICATION: `${BASE_URL}authentication/`,
    ROOT_TRANSACTION: `${BASE_URL}transaction/`,
    ROOT_ATTENDANCE: `${BASE_URL}attendance/`,
    ROOT_INTEGRATION: `${BASE_URL}in/`,
    ROOT_MASTER: `${BASE_URL}master/`,
}

// API List
export const APIList = {
    API_LOGIN: `${APIRoot.ROOT_AUTHENTICATION}login`,
    API_LOGOUT: `${APIRoot.ROOT_AUTHENTICATION}logout`,


    API_GET_GR_PO: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_receipt/po_view`,
    API_GET_GR_PO_DETAIL: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_receipt/po_detail`,
    API_GET_GR_PO_MATERIAL: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_receipt/gr_material`,
    API_GET_GR_MATERIAL_INFO: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_receipt/gr_material_info`,
    API_GET_GR_SUBMIT: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_receipt/submit`,

    API_GET_GI_PO: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_issue/po_view`,
    API_GET_GI_PO_DETAIL: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_issue/po_detail`,
    API_GET_GI_MATERIAL_INFO: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_issue/gi_detail`,
    API_GET_GI_CHECK_QR: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_issue/scan_qr`,
    API_GET_GI_SUBMIT: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_issue/submit`,

    API_GET_TP_VIEW: `${APIRoot.ROOT_TRANSACTION}good_movement/transfer_posting/tp_view`,
    API_GET_TP_DETAIL: `${APIRoot.ROOT_TRANSACTION}good_movement/transfer_posting/tp_detail`,
    API_GET_GI_CHECK_QR_TP: `${APIRoot.ROOT_TRANSACTION}good_movement/transfer_posting/scan_qr`,
    API_SUBMIT_TP: `${APIRoot.ROOT_TRANSACTION}good_movement/transfer_posting/submit`,

    API_GET_PID_VIEW: `${APIRoot.ROOT_TRANSACTION}pid/pid_view`,
    API_GET_PID_DETAIL: `${APIRoot.ROOT_TRANSACTION}pid/pid_view_detail`,
    API_GET_PID_DETAIL_MATERIAL: `${APIRoot.ROOT_TRANSACTION}pid/pid_view_material_detail`,
    API_GET_PID_CHECK_QR: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_receipt/scan_qr`,
    API_SUBMIT_PID: `${APIRoot.ROOT_TRANSACTION}pid/submit`,

    API_GR_HEADER_HISTORY: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_receipt/history_header`,
    API_GR_DETAIL_HISTORY: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_receipt/history_detail`,
    API_GI_HEADER_HISTORY: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_issue/history_header`,
    API_GI_DETAIL_HISTORY: `${APIRoot.ROOT_TRANSACTION}purchase_order/good_issue/history_detail`,
    API_TP_HEADER_HISTORY: `${APIRoot.ROOT_TRANSACTION}good_movement/transfer_posting/history_header`,
    API_TP_DETAIL_HISTORY: `${APIRoot.ROOT_TRANSACTION}good_movement/transfer_posting/history_detail`,
    API_PID_HEADER_HISTORY: `${APIRoot.ROOT_TRANSACTION}pid/history_header`,
    API_PID_DETAIL_HISTORY: `${APIRoot.ROOT_TRANSACTION}pid/history_detail`,
    API_PID_DETAIL_MATERIAL_HISTORY: `${APIRoot.ROOT_TRANSACTION}pid/history_detail_material`,
    
    API_REPORT_SUBMIT: `${APIRoot.ROOT_TRANSACTION}pid/manual_submit`,
    
    API_GET_SLOC: `${APIRoot.ROOT_MASTER}sloc/get`,

    API_RETRIEVE_WORK_PLAN: `${APIRoot.ROOT_ATTENDANCE}retrieve`,

    API_UPLOAD: `${APIRoot.ROOT_INTEGRATION}upload`,
}
