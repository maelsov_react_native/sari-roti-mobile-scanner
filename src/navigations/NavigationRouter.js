import React, { PureComponent } from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TouchableNativeFeedback,
    Platform,
    StyleSheet,
    BackHandler,
    Easing
} from 'react-native'
import { Scene, Router, Stack, Actions } from 'react-native-router-flux'
import AntDesign from 'react-native-vector-icons/AntDesign'

import { Fonts, Icons, Colors, Metrics } from '../globals/GlobalConfig'

import SplashScreen from '../containers/Utility/SplashScreen'
import LoginScreen from '../containers/Auth/Login/LoginScreen'

import GRScreen from '../containers/MainTabs/GR/GRScreen'
import DetailGRScreen from '../containers/MainTabs/GR/DetailGR/DetailGRScreen'
import DetailMaterialGRScreen from '../containers/MainTabs/GR/DetailGR/DetailMaterialGRScreen'

import GIScreen from '../containers/MainTabs/GI/GIScreen'
import DetailGIScreen from '../containers/MainTabs/GI/DetailGI/DetailGIScreen'
import DetailMaterialGIScreen from '../containers/MainTabs/GI/DetailGI/DetailMaterialGIScreen'

import ReportScreen from '../containers/MainTabs/Report/ReportScreen'

import AkunScreen from '../containers/MainTabs/Akun/AkunScreen'
import { getDate, wait } from '../globals/GlobalFunction'
import GlobalStyle from '../globals/GlobalStyle'
import DetailGRScreen2 from '../containers/MainTabs/GR/DetailGR/DetailGRScreen2'
import TransferPostingScreen from '../containers/MainTabs/TransferPosting/TransferPostingScreen'
import DetailMaterialTransferPosting from '../containers/MainTabs/TransferPosting/DetailTransferPosting/DetailMaterialTransferPosting'
import DetailTransferPosting from '../containers/MainTabs/TransferPosting/DetailTransferPosting/DetailTransferPostingScreen'
import DetailMaterialStockOpname from '../containers/MainTabs/SO/DetailSO/DetailMaterialSOScreen'
import StockOpnameScreen from '../containers/MainTabs/SO/SOScreen'
import DetailStockOpnameScreen from '../containers/MainTabs/SO/DetailSO/DetailSOScreen'
import DetailMaterialStockOpnameScreen2 from '../containers/MainTabs/SO/DetailSO/DetailMaterialSOScreen2'
import DetailHistoryGRScreen from '../containers/MainTabs/Report/GRHistory/DetailHistoryGRScreen'
import DetailHistoryGRMaterialScreen from '../containers/MainTabs/Report/GRHistory/DetailHistoryGRMaterialScreen'
import DetailHistoryGIScreen from '../containers/MainTabs/Report/GIHistory/DetailHistoryGIScreen'
import DetailHistoryMaterialGIScreen from '../containers/MainTabs/Report/GIHistory/DetailHistoryGIMaterialScreen'
import DetailHistoryTPScreen from '../containers/MainTabs/Report/TPHistory/DetailHistoryTPScreen'
import DetailHistoryTPMaterialScreen from '../containers/MainTabs/Report/TPHistory/DetailHistoryTPMaterialScreen'
import DetailPIDScreen from '../containers/MainTabs/Report/PIDHistory/DetailHistoryPIDScreen'
import DetailPIDMaterialScreen from '../containers/MainTabs/Report/PIDHistory/DetailHistoryPIDMaterialScreen'

class TabIcon extends PureComponent {
    navigate(pathName) {
        Actions.jump(pathName)
    }

    render() {
        const focusedColor = Colors.BLUE
        const inactiveColor = '#B7B7B7'
        const { screenKey, iconProps } = this.props
        const { focused, title } = iconProps

        let iconTab, titleTab = title

        switch (screenKey) {
            case 'gr':
                iconTab = Icons.iconGoodReceipt
                titleTab = 'Good Receipt'
                break;
            case 'gi':
                iconTab = Icons.iconGoodIssue
                titleTab = 'Good Issue'
                break;
            case 'so':
                iconTab = Icons.iconTabStockOpname
                titleTab = 'Stock Opname'
                break;
            case 'transferPosting':
                iconTab = Icons.iconTabHistory
                titleTab = 'Transfer Posting'
                break;
            case 'report':
                iconTab = Icons.iconTabReport
                titleTab = 'Report'
                break;
            default:
                break;
        }

        return (
            <TouchableNativeFeedback
                onPress={() => this.navigate(screenKey)}
                background={Platform.Version >= 21 ?
                    TouchableNativeFeedback.Ripple(Colors.BLUE_LIGHT, true) :
                    TouchableNativeFeedback.SelectableBackground()}>
                <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', zIndex: 1 }}>
                    <View style={{ height: 24, aspectRatio: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Image resizeMethod="resize" source={iconTab} style={[{ height: '100%', width: '100%', resizeMode: "contain" }, focused ? { tintColor: focusedColor } : { tintColor: inactiveColor }]} />
                    </View>
                    <Text style={[{ fontFamily: Fonts.ROBOTO_MEDIUM, fontSize: 10, marginTop: 5, textAlign: 'center' }, focused ? { color: focusedColor } : { color: inactiveColor }]}>{titleTab}</Text>
                </View>
            </TouchableNativeFeedback>
        )
    }
}

class BackIcon extends PureComponent {
    render() {
        const { handleBack } = this.props
        return (
            <TouchableOpacity onPress={handleBack}>
                <View style={styles.headerBackContainer}>
                    <AntDesign name='arrowleft' size={24} />
                </View>
            </TouchableOpacity>
        )
    }
}

const NavigationRouter = () => {
    const handleBack = () => {
        switch (Actions.currentScene) {
            case 'splash':
            case 'login':
            case 'akun':
                Actions.tabBar()
                break;
            default:
                Actions.pop()
                break;
        }

        return true;
    }

    const renderRightButton = (screen) => () => {
        let image, onIconPress, iconStyle
        switch (screen) {
            case 'gr':
                onIconPress = () => Actions.akun()
                break;
            case 'gi':
                onIconPress = () => Actions.akun()
                break;
            case 'so':
                onIconPress = () => Actions.akun()
                break;
            case 'transferPosting':
                onIconPress = () => Actions.akun()
                break;
            case 'report':
                onIconPress = () => Actions.akun()
                break;
            default:
                break;
        }
        return (
            <TouchableOpacity onPress={onIconPress}>
                <View style={styles.headerIconContainer}>
                    <Image resizeMethod="resize" source={Icons.iconAuthor} style={{ height: 35, width: 35 }} />
                </View>
            </TouchableOpacity>
        )
    }

    const MyTransitionSpec = ({
        duration: 250,
        easing: Easing.bezier(0.2833, 0.99, 0.31833, 0.99)
        // timing: Animated.timing,
    });

    const transitionConfig = () => ({
        transitionSpec: MyTransitionSpec,
        screenInterpolator: sceneProps => {
            const { layout, position, scene } = sceneProps;
            const { index } = scene;
            const width = layout.initWidth;

            // right to left by replacing bottom scene
            return {
                transform: [{
                    translateX: position.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [width, 0, -width],
                    }),
                }]
            };
        }
    });

    return (
        <Router backAndroidHandler={handleBack}>
            <Stack key='root'
                navigationBarStyle={{ ...GlobalStyle.navigationBarShadow, height: Metrics.NAVBAR_HEIGHT }}
                transitionConfig={transitionConfig}
                renderRightButton
                renderBackButton={() => <BackIcon handleBack={handleBack} />}>
                <Scene key='splash'
                    initial
                    hideNavBar
                    type="reset"
                    component={SplashScreen}
                />
                <Scene key='login'
                    hideNavBar
                    type="reset"
                    component={LoginScreen}
                />
                <Scene
                    key='akun'
                    back
                    type="reset"
                    component={AkunScreen}
                    title="Account"
                    titleStyle={styles.headerBackTitle}
                />
                <Scene
                    back
                    key='detailGR'
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    title="Good Receipt Detail"
                    titleStyle={styles.headerBackTitle}
                    component={DetailGRScreen} />
                <Scene
                    back
                    key='detailGR2'
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    title="Good Receipt Detail"
                    titleStyle={styles.headerBackTitle}
                    component={DetailGRScreen2} />
                <Scene
                    back
                    key='detailMaterialGR'
                    title="Good Receipt Material"
                    titleStyle={styles.headerBackTitle}
                    component={DetailMaterialGRScreen} />
                <Scene
                    back
                    key='detailGI'
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    title="Good Issue"
                    titleStyle={styles.headerBackTitle}
                    component={DetailGIScreen} />
                <Scene
                    back
                    key='detailMaterialGI'
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    title="Good Issue Material"
                    titleStyle={styles.headerBackTitle}
                    component={DetailMaterialGIScreen} />
                <Scene
                    back
                    key='detailTransferPosting'
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    title="Transfer Posting Detail"
                    titleStyle={styles.headerBackTitle}
                    component={DetailTransferPosting} />
                <Scene
                    back
                    key='detailMaterialTransferPosting'
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    title="Transfer Posting Material"
                    titleStyle={styles.headerBackTitle}
                    component={DetailMaterialTransferPosting} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailSO'
                    title="Stock Opname Detail"
                    titleStyle={styles.headerBackTitle}
                    component={DetailStockOpnameScreen} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailMaterialSO'
                    title="Stock Opname Material Detail"
                    titleStyle={styles.headerBackTitle}
                    component={DetailMaterialStockOpname} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailMaterialSO2'
                    title="Stock Opname Material Detail"
                    titleStyle={styles.headerBackTitle}
                    component={DetailMaterialStockOpnameScreen2} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailHistoryGR'
                    title="GR Detail History"
                    titleStyle={styles.headerBackTitle}
                    component={DetailHistoryGRScreen} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailHistoryMatrerialGR'
                    title="GR Detail History"
                    titleStyle={styles.headerBackTitle}
                    component={DetailHistoryGRMaterialScreen} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailHistoryGI'
                    title="GI Detail History"
                    titleStyle={styles.headerBackTitle}
                    component={DetailHistoryGIScreen} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailHistoryMaterialGI'
                    title="GI Detail History"
                    titleStyle={styles.headerBackTitle}
                    component={DetailHistoryMaterialGIScreen} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailHistoryTP'
                    title="TP Detail History"
                    titleStyle={styles.headerBackTitle}
                    component={DetailHistoryTPScreen} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailHistoryMaterialTP'
                    title="TP Detail History"
                    titleStyle={styles.headerBackTitle}
                    component={DetailHistoryTPMaterialScreen} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailHistoryPID'
                    title="PID Detail History"
                    titleStyle={styles.headerBackTitle}
                    component={DetailPIDScreen} />
                <Scene
                    back
                    onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                    key='detailHistoryPID2'
                    title="Material Detail History"
                    titleStyle={styles.headerBackTitle}
                    component={DetailPIDMaterialScreen} />
                <Scene key="tabBar"
                    type="reset"
                    tabs
                    showLabel={false}
                    tabBarStyle={{ backgroundColor: Colors.WHITE, borderTopWidth: 1, borderTopColor: Colors.GRAY, height: 70 }}
                    hideNavBar>
                    <Scene key='gr'
                        onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                        renderRightButton={() => renderRightButton("gr")}
                        title="Good Receipt"
                        titleStyle={styles.headerLeftTitle}
                        icon={(iconProps) => <TabIcon screenKey='gr' iconProps={iconProps} />}
                        component={GRScreen} />
                    <Scene key='gi'
                        onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                        renderRightButton={() => renderRightButton("gi")}
                        title="Good Issue"
                        titleStyle={styles.headerLeftTitle}
                        icon={(iconProps) => <TabIcon screenKey='gi' iconProps={iconProps} />}
                        component={GIScreen} />
                    <Scene key='transferPosting'
                        onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                        renderRightButton={() => renderRightButton("transferPosting")}
                        title="Transfer Posting"
                        titleStyle={styles.headerLeftTitle}
                        icon={(iconProps) => <TabIcon screenKey='transferPosting' iconProps={iconProps} />}
                        component={TransferPostingScreen} />
                    <Scene key='so'
                        onEnter={() => Actions.refresh({ lastUpdate: new Date })}
                        renderRightButton={() => renderRightButton("so")}
                        title="Stock Opname"
                        titleStyle={styles.headerLeftTitle}
                        icon={(iconProps) => <TabIcon screenKey='so' iconProps={iconProps} />}
                        component={StockOpnameScreen} />
                    <Scene key='report'
                        renderRightButton={() => renderRightButton("report")}
                        title="Report"
                        titleStyle={styles.headerLeftTitle}
                        icon={(iconProps) => <TabIcon screenKey='report' iconProps={iconProps} />}
                        component={ReportScreen} />
                </Scene>
            </Stack>
        </Router>
    )
}

const styles = StyleSheet.create({
    headerBackContainer: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerIconContainer: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerLeftTitle: {
        paddingLeft: 3,
        color: Colors.BLACK,
        fontSize: 16,
        lineHeight: 21,
        letterSpacing: 0.25,
        fontFamily: Fonts.ROBOTO_REGULAR,
    },
    headerBackTitle: {
        marginLeft: 0,
        color: Colors.BLACK,
        fontSize: 16,
        lineHeight: 21,
        letterSpacing: 0.25,
        fontFamily: Fonts.ROBOTO_REGULAR,
    },
})

export default NavigationRouter