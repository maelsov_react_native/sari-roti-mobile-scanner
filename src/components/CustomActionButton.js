import React from 'react'
import { View, Image, TouchableNativeFeedback } from 'react-native'

import { Colors, Icons } from '../globals/GlobalConfig'

const DEFAULT_BUTTON_COLOR = Colors.BLUE
const DEFAULT_ICON = Icons.iconEdit
const DEFAULT_ICON_COLOR = Colors.WHITE

export default (props) => {
    const {
        size = 54,
        iconSize = 20,
        edge = 24,
        color = DEFAULT_BUTTON_COLOR,
        onClick
    } = props

    return (
        <View style={{
            height: size,
            aspectRatio: 1,
            borderRadius: size / 2,
            position: 'absolute',
            bottom: edge,
            right: edge,

            backgroundColor: color,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0.22,
            shadowRadius: 2.22,

            elevation: 3,
            overflow: 'hidden'
        }}>
            <TouchableNativeFeedback
                onPress={onClick}
                background={TouchableNativeFeedback.Ripple(DEFAULT_ICON_COLOR)}>
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <Image source={DEFAULT_ICON} resizeMode='contain' style={{ height: iconSize, aspectRatio: 1, tintColor: DEFAULT_ICON_COLOR }} />
                </View>
            </TouchableNativeFeedback>
        </View>
    )
}
