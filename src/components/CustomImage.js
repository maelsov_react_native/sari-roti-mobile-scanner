import React, { useEffect, useRef, useState } from 'react'
import { View, Image, Easing, Animated, TouchableNativeFeedback, TouchableOpacity, ScrollView, Text } from 'react-native'
import Modal from 'react-native-modal';
import AntDesign from 'react-native-vector-icons/AntDesign'

import { Colors, Fonts, Icons, Metrics } from '../globals/GlobalConfig'
import { wait } from '../globals/GlobalFunction';

const CustomImage = (props) => {
    const {
        source,
        size,
        containerStyle,
        onEditChosenPhoto,
        description = ''
    } = props
    const [isOpen, setIsOpen] = useState(false)
    const [currX, setCurrX] = useState(0)
    const [currY, setCurrY] = useState(0)

    const [animatedBackground] = useState(new Animated.Value(0))
    const [animatedImage] = useState(new Animated.Value(0))

    const imageRef = useRef(null);

    useEffect(() => {
        setIsOpen(false)
    }, [source])

    useEffect(() => {
        imageRef.current.measure((fx, fy, width, height, px, py) => {
            // console.log('Component width is: ' + width)
            // console.log('Component height is: ' + height)
            // console.log('X offset to frame: ' + fx)
            // console.log('Y offset to frame: ' + fy)
            // console.log('X offset to page: ' + px)
            // console.log('Y offset to page: ' + py)
            setCurrX(px)
            setCurrY(py + 10)
        })
    }, [props.scrollPosition])

    const handleAnimation = () => {
        if (!isOpen) setIsOpen(!isOpen)
        else wait(400).then(() => setIsOpen(!isOpen))
        Animated.timing(animatedImage, {
            toValue: isOpen ? 0 : 1,
            duration: 400,
            easing: Easing.ease,
            useNativeDriver: true
        }).start()
        Animated.timing(animatedBackground, {
            toValue: isOpen ? 0 : 1,
            duration: 400,
            easing: Easing.ease,
            useNativeDriver: false
        }).start()
    }

    const modalContent = (
        <Animated.View style={{
            height: Metrics.SCREEN_HEIGHT,
            width: Metrics.SCREEN_WIDTH,
            backgroundColor: animatedBackground.interpolate({
                inputRange: [0, 0.7, 1],
                outputRange: [`${Colors.WHITE}00`, `${Colors.BLACK}EE`, `${Colors.BLACK}EE`]
            })
        }}>
            <TouchableNativeFeedback onPress={handleAnimation} disabled={!isOpen}>
                <Animated.View style={{
                    position: 'absolute',
                    top: 10,
                    height: Metrics.NAVBAR_HEIGHT,
                    aspectRatio: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    opacity: animatedImage
                }}>
                    <AntDesign name='arrowleft' size={24} color={Colors.WHITE} />
                </Animated.View>
            </TouchableNativeFeedback>
            <Animated.View
                style={{
                    height: size,
                    aspectRatio: 1,
                    opacity: animatedImage.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 1]
                    }),
                    transform: [
                        {
                            translateX: animatedImage.interpolate({
                                inputRange: [0, 1],
                                outputRange: [currX, Metrics.SCREEN_WIDTH / 2 - size / 2]
                            })
                        },
                        {
                            translateY: animatedImage.interpolate({
                                inputRange: [0, 1],
                                outputRange: [currY, Metrics.SCREEN_HEIGHT / 2 - size / 2]
                            })
                        },
                        {
                            scale: animatedImage.interpolate({
                                inputRange: [0, 1],
                                outputRange: [1, (Metrics.SCREEN_WIDTH) / size]
                            })
                        }
                    ]
                }}
            >
                <Image
                    source={source ? source : Icons.iconCamera}
                    resizeMode='contain'
                    style={{ flex: 1 }} />
            </Animated.View>
            <Animated.View style={{
                position: 'absolute',
                bottom: 0,
                width: '100%',
                paddingBottom: 30,
                paddingHorizontal: 30,
                height: Metrics.SCREEN_HEIGHT / 4,
                opacity: animatedImage
            }}>
                <View style={{ marginBottom: 10 }}>
                    <Text style={{ fontSize: 14, fontFamily: Fonts.ROBOTO_MEDIUM, fontWeight: 'bold', color: Colors.WHITE }}>Keterangan Foto</Text>
                </View>
                <ScrollView>
                    <Text style={{ fontSize: 14, fontFamily: Fonts.ROBOTO_REGULAR, lineHeight: 18, color: Colors.WHITE }}>{description != '' ? description : 'Belum ada keterangan foto'}</Text>
                </ScrollView>
            </Animated.View>
        </Animated.View>
    )

    return (
        <View style={containerStyle}>
            <TouchableOpacity
                ref={imageRef}
                onPress={source ? handleAnimation : onEditChosenPhoto}
                style={[{ height: size, aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }, !source && { backgroundColor: Colors.GRAY, paddingRight: 5 }]}
            >
                <Image
                    source={source ? source : Icons.iconCamera}
                    resizeMode={source ? 'cover' : 'contain'}
                    style={[
                        { aspectRatio: 1 },
                        source ? { height: size } : { height: size / 2, tintColor: Colors.WHITE }
                    ]} />
            </TouchableOpacity>
            <Modal
                hideModalContentWhileAnimating
                animationIn='fadeIn'
                animationOut='fadeOut'
                animationInTiming={0.1}
                animationOutTiming={0.1}
                isVisible={isOpen}
                style={{ margin: 0 }}
                hasBackdrop={false}
                onBackButtonPress={handleAnimation}
                children={isOpen ? modalContent : <View />} />
        </View>
    )
}

export default CustomImage