import React from 'react'
import { View, ActivityIndicator, Text } from 'react-native'
import { Colors, Fonts } from '../globals/GlobalConfig'

const DEFAULT_LOADING_COLOR = Colors.BLUE
const LOADING_LABEL_FONT = Fonts.ROBOTO_MEDIUM

export default (props) => {
    const { loadingColor = DEFAULT_LOADING_COLOR, loadingSize = 'large', label = '' } = props

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size={loadingSize} color={loadingColor} />
            {label != '' && (
                <View style={{ marginTop: 20 }}>
                    <Text style={{ fontFamily: LOADING_LABEL_FONT, color: DEFAULT_LOADING_COLOR, textAlign: 'center', fontSize: 12 }}>{label}</Text>
                </View>
            )}
        </View>
    )
}
