import React, { PureComponent } from 'react';
import {
    View,
    Text,
    Platform,
    StyleSheet,
    TouchableNativeFeedback
} from 'react-native';
import Modal from 'react-native-modal';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import GlobalStyle from '../globals/GlobalStyle';
import { Colors, Fonts } from '../globals/GlobalConfig';

export default class CustomModalError extends PureComponent {
    render() {
        const {
            title,
            body,
            onConfirm,
            onCancel,
            isModalVisible
        } = this.props

        const modalContent = (
            <View style={styles.modalContainer}>
                <View style={styles.modalHeaderContainer}>
                    <Text style={GlobalStyle.modalHeaderText}>{title}</Text>
                    <TouchableNativeFeedback onPress={onCancel} style={{paddingLeft: 30}}>
                        <FontAwesome name="close" style={{fontSize: 22, color: "#fff", textAlign: "right"}}/>
                    </TouchableNativeFeedback>
                </View>
                <View style={styles.modalContentContainer}>
                    <Text style={styles.modalContent}>{body}</Text>
                </View>
            </View>
        )

        return (
            <Modal
                avoidKeyboard={Platform.OS === "ios" ? true : false}
                isVisible={isModalVisible}
                style={{ margin: 30 }}
                backdropOpacity={0.8}
                children={modalContent} />
        );
    }
}

const styles = StyleSheet.create({
    modalContainer: {
        backgroundColor: Colors.WHITE,
        borderRadius: 10,
        overflow: 'hidden'
    },
    modalHeaderContainer: {
        backgroundColor: Colors.GREEN,
        paddingVertical: 20,
        paddingHorizontal: 30,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    modalContentContainer: {
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
    modalTitle: {
        ...GlobalStyle.textFontSize16,
        fontFamily: Fonts.ROBOTO_MEDIUM,
        fontWeight: "bold",
        color: Colors.WHITE
    },
    modalContent: {
        ...GlobalStyle.textFontSize14,
        fontFamily: Fonts.ROBOTO_REGULAR,
        color: Colors.DARK,
        lineHeight: 20
    },
    buttonContainer: {
        flex: 1,
        height: 36,
        flexDirection: 'row',
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    buttonText: {
        ...GlobalStyle.textFontSize14,
        fontFamily: Fonts.ROBOTO_MEDIUM,
        fontWeight: "bold",
        letterSpacing: 0.2,
        color: Colors.WHITE
    },
    iconStyle: {
        position: 'absolute',
        left: 8,
        top: 6,
        fontSize: 24,
        color: Colors.WHITE
    }
})
