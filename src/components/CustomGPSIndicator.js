import React from 'react'
import { ActivityIndicator, View, Text } from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { Colors } from '../globals/GlobalConfig'
import GlobalStyle from '../globals/GlobalStyle'

const CustomGPSIndicator = (props) => {
    const { isLoadingGPS, latitude, longitude } = props

    return (
        <View style={[GlobalStyle.detailContainer, { alignItems: "center", flexDirection: "row" }]}>
            <View style={{ flex: 1, justifyContent: "center" }}>
                <Text style={GlobalStyle.contentCardTextLabel}>GPS Geolocation</Text>
            </View>
            <View style={[{ flex: 1, marginBottom: 2 }, !isLoadingGPS && { paddingLeft: 20 }]}>
                {isLoadingGPS ? (
                    <View style={{ width: 40 }}>
                        <ActivityIndicator color={Colors.GREEN_DARK} />
                    </View>
                )
                    : <FontAwesome
                        name={latitude != "" && longitude != "" ? "check" : 'close'}
                        color={latitude != "" && longitude != "" ? Colors.GREEN : Colors.RED}
                        size={16} />
                }
            </View>
        </View>
    )
}

export default CustomGPSIndicator